using AutoMapper;
using BakeryApi.Core;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.DAL.Context;
using BakeryApi.DAL.UnitOfWork;
using BakeryApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

namespace BakeryApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("DatabaseConection");
            services.AddDbContext<BakeryContext>(e => e.UseSqlServer(connection));

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<ICustomersService, CustomersService>();
            services.AddScoped<IEmployeesService, EmployeesService>();
            services.AddScoped<IIngredientsService, IngredientsService>();
            services.AddScoped<IMeasureService, MeasureService>();
            services.AddScoped<IPositionsService, PositionService>();
            services.AddScoped<IProductOnStorageService, ProductOnStorageService>();
            services.AddScoped<IProductOrderItemService, ProductOrderItemService>();
            services.AddScoped<IProductsOrdersService, ProductOrdersService>();
            services.AddScoped<IProductsService, ProductsService>();
            services.AddScoped<IRemovedProductsService, RemovedProductsService>();
            services.AddScoped<ISuppliersService, SuppliersService>();
            services.AddScoped<IRawStaffService, RawStaffService>();
            services.AddScoped<IRawStaffOnStorageService, RawStaffOnStorageService>();
            services.AddScoped<IRawStaffOrderItemService, RawStaffOrderItemService>();
            services.AddScoped<IRawStaffOrderService, RawStaffOrderService>();
            services.AddScoped<IRemovedRawStaffService, RemovedRawStaffService>();



            services.AddCors(options =>
            {
                options.AddPolicy("testPolicy", policy =>
                {
                    policy.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("testPolicy");

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
