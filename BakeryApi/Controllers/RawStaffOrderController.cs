﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RawStaffOrderController : Controller
    {
        private readonly IRawStaffOrderService _rawStaffOrderService;
        private readonly IRawStaffOrderItemService _ordersItemsService;

        public RawStaffOrderController(IRawStaffOrderService rawStaffOrderService, IRawStaffOrderItemService ordersItemsService)
        {
            _rawStaffOrderService = rawStaffOrderService;
            _ordersItemsService = ordersItemsService;
        }

        [HttpGet]
        public ActionResult<List<RawStaffOrderDTO>> GetAllOrders()
        {
            return _rawStaffOrderService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<RawStaffOrderDTO> GetById(int id)
        {
            var result = _rawStaffOrderService.GetById(id);

            return result;
        }

        [HttpGet("BySupplier/{supplierId}")]
        public ActionResult<List<RawStaffOrderDTO>> GetBySupplierId(int supplierId)
        {
            var result = _rawStaffOrderService.GetBySupplierId(supplierId);

            return result;
        }

        [HttpGet("GetEmployees/{orderId}")]
        public ActionResult<List<EmployeeDTO>> GetEmployeesByOrderId(int orderId)
        {
            var result = _rawStaffOrderService.GetEmployeesByOrderId(orderId);

            return result;
        }

        [HttpPost]
        public ActionResult<RawStaffOrderDTO> Insert(RawStaffOrders product)
        {
            var result = _rawStaffOrderService.Insert(product);

            return result;
        }

        [HttpPut]
        public ActionResult<RawStaffOrderDTO> Update(RawStaffOrders product)
        {
            var result = _rawStaffOrderService.Update(product);

            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _rawStaffOrderService.Delete(id);

            return NoContent();
        }


        [HttpGet("OrderItem/{id}")]
        public ActionResult<RawStaffOrderItemDTO> GetOrderItemsById(int id)
        {
            var result = _ordersItemsService.GetById(id);

            return result;
        }

        [HttpGet("OrderItem/ByRawStaff/{orderId}")]
        public ActionResult<List<RawStaffOrderItemDTO>> GetOrderItemByOrderId(int orderId)
        {
            var result = _ordersItemsService.GetByOrderId(orderId);

            return result;
        }

        [HttpGet("OrderItem/All")]
        public ActionResult<List<RawStaffOrderItemDTO>> GetAllOrderItems()
        {
            var result = _ordersItemsService.GetAll();

            return result;
        }

        [HttpPost("OrderItem")]
        public ActionResult<RawStaffOrderItemDTO> InsertOrderItem(RawStaffOrderItemDTO orderItem)
        {
            orderItem = _ordersItemsService.Insert(orderItem);

            return orderItem;
        }

        [HttpPost("OrderItem/AddRange")]
        public ActionResult<List<RawStaffOrderItemDTO>> InsertRangeOrderItems(List<RawStaffOrderItemDTO> orderItems)
        {
            orderItems = _ordersItemsService.InsertRange(orderItems);

            return orderItems;
        }

        [HttpPut("OrderItem")]
        public ActionResult<RawStaffOrderItemDTO> UpdateOrderItem(RawStaffOrderItemDTO orderItem)
        {
            orderItem = _ordersItemsService.Update(orderItem);

            return orderItem;
        }

        [HttpDelete("OrderItem/{id}")]
        public IActionResult DeleteIngredient(int id)
        {
            _ordersItemsService.Delete(id);

            return NoContent();
        }

    }
}