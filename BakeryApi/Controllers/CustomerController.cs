﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomersService _customersService;

        public CustomerController(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        [HttpGet]
        public ActionResult<List<CustomerDTO>> GetAll()
        {
            var result = _customersService.GetAll();

            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> GetById(int id)
        {
            var result = _customersService.GetById(id);

            return result;
        }

        [HttpPost]
        public ActionResult<CustomerDTO> Insert(CustomerDTO customer)
        {
            customer = _customersService.Insert(customer);

            return customer;
        }

        [HttpPut]
        public ActionResult<CustomerDTO> Update(CustomerDTO customer)
        {
            customer = _customersService.Update(customer);

            return customer;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _customersService.Delete(id);

            return NoContent();
        }
    }
}