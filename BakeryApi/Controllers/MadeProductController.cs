﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MadeProductController : ControllerBase
    {
        private readonly IProductOnStorageService _productOnStorageService;
        private readonly IRemovedProductsService _removedProductsService;

        public MadeProductController(IProductOnStorageService productOnStorageService, IRemovedProductsService removedProductsService)
        {
            _productOnStorageService = productOnStorageService;
            _removedProductsService = removedProductsService;
        }

        [HttpGet]
        public ActionResult<List<ProductOnStorageDTO>> GetAll()
        {
            var result = _productOnStorageService.GetAll();

            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<ProductOnStorageDTO> GetById(int id)
        {
            var result = _productOnStorageService.GetById(id);

            return result;
        }

        [HttpPost]
        public ActionResult<ProductOnStorageDTO> Insert(InsertingProductOnStorageDTO product)
        {
            var result = _productOnStorageService.Insert(product);

            return result;
        }

        [HttpGet("Employees{id}")]
        public ActionResult<List<EmployeeDTO>> GetEmployeesByProductId(int id)
        {
            var result = _productOnStorageService.GetEmployeesByProductId(id);

            return result;
        }

        [HttpGet("Removed/All")]
        public ActionResult<List<RemovedProductDTO>> GetAllremovedProduct()
        {
            var result = _removedProductsService.GetAll();

            return result;
        }
        [HttpGet("Removed/{removedId}")]
        public ActionResult<RemovedProductDTO> GetRemovedById(int removedId)
        {
            var result = _removedProductsService.GetById(removedId);

            return result;
        }

        [HttpPost("RemovedProductFromStorage")]
        public ActionResult<RemovedProductDTO> RemoveProduct(RemovedProductDTO product, int productOnStorageId)
        {
            var result = _removedProductsService.RemoveFromStorage(product, productOnStorageId);

            return result;
        }

        [HttpDelete("Removed/{id}")]
        public IActionResult DeleteRemovedProduct(int id)
        {
            _removedProductsService.Delete(id);

            return NoContent();
        }
    }
}