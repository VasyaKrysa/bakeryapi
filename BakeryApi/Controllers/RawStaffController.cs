﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RawStaffController : ControllerBase
    {
        private readonly IRawStaffService _rawStaffService;
        private readonly IRawStaffOnStorageService _rawStaffOnStorageService;
        private readonly IRemovedRawStaffService _removedRawStaffService;

        public RawStaffController(  IRawStaffService rawStaffService, 
                                    IRawStaffOnStorageService rawStaffOnStorageService,
                                    IRemovedRawStaffService removedRawStaffService)
        {
            _rawStaffService = rawStaffService;
            _rawStaffOnStorageService = rawStaffOnStorageService;
            _removedRawStaffService = removedRawStaffService;
        }

        [HttpGet]
        public ActionResult<List<RawStaffDTO>> GetAll()
        {
            var result = _rawStaffService.GetAll();

            return result;
        }

        [HttpGet("OnStorage")]
        public ActionResult<List<RawStaffOnStorageDTO>> GetAllRawStaffOnStorage()
        {
            var result = _rawStaffOnStorageService.GetAll();

            return result;
        }

        [HttpGet("OnStorage/{id}")]
        public ActionResult<RawStaffOnStorageDTO> GetByIdRawStaffOnStorage(int id)
        {
            var result = _rawStaffOnStorageService.GetById(id);

            return result;
        }

        [HttpGet("Removed")]
        public ActionResult<List<RemovedRawStaffDTO>> GetAllRemovedRawStaff()
        {
            var result = _removedRawStaffService.GetAll();

            return result;
        }

        [HttpGet("Removed/{id}")]
        public ActionResult<RemovedRawStaffDTO> GetByIdRemovedRawStaff(int id)
        {
            var result = _removedRawStaffService.GetById(id);

            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<RawStaffDTO> GetById(int id)
        {
            var result = _rawStaffService.GetById(id);

            return result;
        }

        [HttpGet("GetByName/{name}")]
        public ActionResult<List<RawStaffDTO>> GetByRawStaffName(string name)
        {
            var result = _rawStaffService.GetByName(name);

            return result;
        }

        [HttpPost]
        public ActionResult<RawStaffDTO> Insert(RawStaffDTO rawStaff)
        {
            rawStaff = _rawStaffService.Insert(rawStaff);

            return rawStaff;
        }

        [HttpPost("RemoveFromStorage")]
        public ActionResult<RemovedRawStaffDTO> RemoveProduct(RemovedRawStaffDTO rawStaff, int rawStaffOnStorageID)
        {
            var result = _removedRawStaffService.RemoveFromStorage(rawStaff, rawStaffOnStorageID);

            return result;
        }

        [HttpPut]
        public ActionResult<RawStaffDTO> Update(RawStaffDTO rawStaff)
        {
            rawStaff = _rawStaffService.Update(rawStaff);

            return rawStaff;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _rawStaffService.Delete(id);

            return NoContent();
        }

     
    }
}