﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductsService _productsService;
        private readonly IIngredientsService _ingredientsService;

        public ProductController(IProductsService productsService, IIngredientsService ingredientsService)
        {
            _productsService = productsService;
            _ingredientsService = ingredientsService;
        }
        [HttpGet]
        public ActionResult<List<ProductDTO>> GetAll()
        {
            var result = _productsService.GetAll();

            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<ProductDTO> GetById(int id)
        {
            var result = _productsService.GetById(id);

            return result;
        }

        [HttpPost]
        public ActionResult<ProductDTO> Insert(ProductDTO product)
        {
            _productsService.Insert(product);

            return product;
        }

        [HttpPut]
        public ActionResult<ProductDTO> Update(ProductDTO product)
        {
            _productsService.Update(product);

            return product;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productsService.Delete(id);

            return NoContent();
        }

        [HttpGet("Ingredient/{id}")]
        public ActionResult<IngredientDTO> GetIngredientsById(int id)
        {
            var result = _ingredientsService.GetById(id);

            return result;
        }

        [HttpGet("Ingredient/ByProduct{productId}")]
        public ActionResult<List<IngredientDTO>> GetIngredientsByProductId(int productId)
        {
            var result = _ingredientsService.GetByProductId(productId);

            return result;
        }

        [HttpGet("Ingredient/All")]
        public ActionResult<List<IngredientDTO>> GetAllIngredients()
        {
            var result = _ingredientsService.GetAll();

            return result;
        }

        [HttpPost("Ingredient")]
        public ActionResult<IngredientDTO> InsertIngredient(IngredientDTO ingredient)
        {
            _ingredientsService.Insert(ingredient);

            return ingredient;
        }

        [HttpPost("Ingredient/AddRange")]
        public ActionResult<List<IngredientDTO>> InsertRangeIngredients(List<IngredientDTO> ingredients)
        {
            _ingredientsService.InsertRange(ingredients);
           
            return ingredients;
        }

        [HttpPut("Ingredient")]
        public ActionResult<IngredientDTO> UpdateIngredient(IngredientDTO ingredient)
        {
            _ingredientsService.Update(ingredient);

            return ingredient;
        }

        [HttpDelete("Ingredient/{id}")]
        public IActionResult DeleteIngredient(int id)
        {
            _ingredientsService.Delete(id);

            return NoContent();
        }

    }
}