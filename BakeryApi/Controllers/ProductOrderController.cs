﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductOrderController : ControllerBase
    {
        private readonly IProductsOrdersService _productsOrdersService;
        private readonly IProductOrderItemService _ordersItemsService;

        public ProductOrderController(IProductsOrdersService productsOrdersService, IProductOrderItemService ordersItemsService)
        {
            _productsOrdersService = productsOrdersService;
            _ordersItemsService = ordersItemsService;
        }

        [HttpGet]
        public ActionResult<List<ProductsOrderDTO>> GetAllOrders()
        {
            return _productsOrdersService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<ProductsOrderDTO> GetById(int id)
        {
            var result = _productsOrdersService.GetById(id);

            return result;
        }

        [HttpGet("ByCustomer/{customerId}")]
        public ActionResult<List<ProductsOrderDTO>> GetByCustomerId(int customerId)
        {
            var result = _productsOrdersService.GetByCustomerId(customerId);

            return result;
        }

        [HttpGet("GetEmployees/{orderId}")]
        public ActionResult<List<EmployeeDTO>> GetEmployeesByOrderId(int orderId)
        {
            var result = _productsOrdersService.GetEmployeesByOrderId(orderId);

            return result;
        }

        [HttpPost]
        public ActionResult<ProductsOrderDTO> Insert(ProductOrders product)
        {
            var result = _productsOrdersService.Insert(product);
 
            return result;
        }

        [HttpPut]
        public ActionResult<ProductsOrderDTO> Update(ProductOrders product)
        {
            var result = _productsOrdersService.Update(product);

            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productsOrdersService.Delete(id);

            return NoContent();
        }


        [HttpGet("OrderItem/{id}")]
        public ActionResult<ProductOrderItemDTO> GetOrderItemsById(int id)
        {
            var result = _ordersItemsService.GetById(id);

            return result;
        }

        [HttpGet("OrderItem/ByProduct/{orderId}")]
        public ActionResult<List<ProductOrderItemDTO>> GetOrderItemByOrderId(int orderId)
        {
            var result = _ordersItemsService.GetByOrderId(orderId);

            return result;
        }

        [HttpGet("OrderItem/All")]
        public ActionResult<List<ProductOrderItemDTO>> GetAllOrderItems()
        {
            var result = _ordersItemsService.GetAll();

            return result;
        }

        [HttpPost("OrderItem")]
        public ActionResult<ProductOrderItemDTO> InsertOrderItem(ProductOrderItemDTO orderItem)
        {
            orderItem = _ordersItemsService.Insert(orderItem);

            return orderItem;
        }

        [HttpPost("OrderItem/AddRange")]
        public ActionResult<List<ProductOrderItemDTO>> InsertRangeOrderItems(List<ProductOrderItemDTO> orderItems)
        {
            orderItems = _ordersItemsService.InsertRange(orderItems);

            return orderItems;
        }

        [HttpPut("OrderItem")]
        public ActionResult<ProductOrderItemDTO> UpdateOrderItem(ProductOrderItemDTO orderItem)
        {
            orderItem = _ordersItemsService.Update(orderItem);

            return orderItem;
        }

        [HttpDelete("OrderItem/{id}")]
        public IActionResult DeleteIngredient(int id)
        {
            _ordersItemsService.Delete(id);

            return NoContent();
        }

    }
}