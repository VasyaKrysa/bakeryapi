﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
        [Route("[controller]")]
        [ApiController]
        public class EmployeeController : ControllerBase
        {
            private readonly IEmployeesService _employeesService;

            public EmployeeController(IEmployeesService employeesService)
            {
                _employeesService = employeesService;
            }

            [HttpGet]
            public ActionResult<List<EmployeeDTO>> GetAll()
            {
                var result = _employeesService.GetAll();

                return result;
            }

            [HttpGet("{id}")]
            public ActionResult<EmployeeDTO> GetById(int id)
            {
                var result = _employeesService.GetById(id);

                return result;
            }

            [HttpPost]
            public ActionResult<EmployeeDTO> Insert(EmployeeDTO employee)
            {
                employee = _employeesService.Insert(employee);

                return employee;
            }

            [HttpPut]
            public ActionResult<EmployeeDTO> Update(EmployeeDTO employee)
            {
                employee = _employeesService.Update(employee);

                return employee;
            }

            [HttpDelete("{id}")]
            public IActionResult Delete(int id)
            {
                _employeesService.Delete(id);

                return NoContent();
            }
        }
    }
