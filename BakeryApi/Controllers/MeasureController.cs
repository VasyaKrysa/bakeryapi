﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeasureController : ControllerBase
    {
        private readonly IMeasureService _measureService;

        public MeasureController(IMeasureService measureService)
        {
            _measureService = measureService;
        }

        [HttpGet]
        public ActionResult<List<MeasureDTO>> GetAll()
        {
            var result = _measureService.GetAll();

            return result;
        }

        [HttpGet("{name}")]
        public ActionResult<MeasureDTO> GetById(string name)
        {
            var result = _measureService.GetByName(name);

            return result;
        }

        [HttpPost]
        public ActionResult<MeasureDTO> Insert(MeasureDTO measure)
        {
            measure = _measureService.Insert(measure);

            return measure;
        }

        [HttpPut]
        public ActionResult<MeasureDTO> Update(MeasureDTO measure)
        {
            measure = _measureService.Update(measure);

            return measure;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _measureService.Delete(id);

            return NoContent();
        }
    }
}