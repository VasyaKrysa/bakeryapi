﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BakeryApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ISuppliersService _suppliersService;

        public SupplierController(ISuppliersService suppliersService)
        {
            _suppliersService = suppliersService;
        }

        [HttpGet]
        public ActionResult<List<SupplierDTO>> GetAll()
        {
            var result = _suppliersService.GetAll();

            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<SupplierDTO> GetById(int id)
        {
            var result = _suppliersService.GetById(id);

            return result;
        }

        [HttpGet("{id}/RawStaff")]
        public ActionResult<List<RawStaffDTO>> GetAllRawStaff(int id)
        {
            var result = _suppliersService.GetAllRawStaff(id);

            return result;
        }

        [HttpPost]
        public ActionResult<SupplierDTO> Insert(SupplierDTO supplier)
        {
            supplier = _suppliersService.Insert(supplier);

            return supplier;
        }

        [HttpPut]
        public ActionResult<SupplierDTO> Update(SupplierDTO supplier)
        {
            supplier = _suppliersService.Update(supplier);

            return supplier;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _suppliersService.Delete(id);

            return NoContent();
        }
    }
}