﻿using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.DAL.Context;
using BakeryApi.DAL.EntityRepositories;

namespace BakeryApi.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IAddressRepository _addressRepository;
        private ICustomersRepository _customersRepository;
        private IEmployeesProductOrdersRepository _employeesProductOrdersRepository;
        private IEmployeesRawStaffOrdersRepository _employeesRawStaffOrdersRepository;
        private IEmployeesRawStaffOnStorageRepository _employeesRawStaffOnStorageRepository;
        private IEmployeesRepository _employeesRepository;
        private IIngredientsRepository _ingredientsRepository;
        private IMeasureRepository _measureRepository;
        private IOrderStatusesRepository _orderStatusesRepository;
        private IPositionRepository _positionRepository;
        private IProductOnStorageEmployeesRepository _productOnStorageEmployeesRepository;
        private IProductOnStorageRepository _productOnStorageRepository;
        private IProductOrderItemRepository _poductOrderItemRepository;
        private IProductOrdersRepository _productOrdersRepository;
        private IProductsRepository _productsRepository;
        private IRawStaffOnStorageRepository _rawStaffOnStorageRepository;
        private IRawStaffOrderItemRepository _rawStaffOrderItemRepository;
        private IRawStaffOrdersRepository _rawStaffOrdersRepository;
        private IRawStaffRepository _rawStaffRepository;
        private IRemovedProductsRepository _removedProductsRepository;
        private IRemovedRawStaffRepository _removedRawStaffRepository;
        private IStoragesRepository _storagesRepository;
        private ISuppliersRawStaffRepository _suppliersRawStaffRepository;
        private ISuppliersRepository _suppliersRepository;
        private BakeryContext _context;
        public UnitOfWork(BakeryContext context)
        {
            _context = context;

        }

        public IAddressRepository AddressRepository
        {
            get
            {
                return _addressRepository ??= new AddressRepository(_context);
            }
        }

        public ICustomersRepository CustomersRepository
        {
            get
            {
                return _customersRepository ??= new CustomersRepository(_context);
            }
        }

        public IEmployeesRawStaffOrdersRepository EmployeesRawStaffOrdersRepository
        {
            get
            {
                return _employeesRawStaffOrdersRepository ??= new EmployeesRawStaffOrdersRepository(_context);
            }
        }

        public IEmployeesProductOrdersRepository EmployeesProductOrdersRepository
        {
            get
            {
                return _employeesProductOrdersRepository ??= new EmployeesProductOrderRepository(_context);
            }
        }

        public IEmployeesRawStaffOnStorageRepository EmployeesRawStaffOnStorageRepository
        {
            get
            {
                return _employeesRawStaffOnStorageRepository ??= new EmployeesRawStaffOnStorageRepository(_context);
            }
        }

        public IEmployeesRepository EmployeesRepository
        {
            get
            {
                return _employeesRepository ??= new EmployeesRepository(_context);
            }
        }

        public IIngredientsRepository IngredientsRepository
        {
            get
            {
                return _ingredientsRepository ??= new IngredientsRepository(_context);
            }
        }

        public IMeasureRepository MeasureRepository
        {
            get
            {
                return _measureRepository ??= new MeasureRepository(_context);
            }
        }

        public IOrderStatusesRepository OrderStatusesRepository
        {
            get
            {
                return _orderStatusesRepository ??= new OrderStatusesRepository(_context);
            }
        }

        public IPositionRepository PositionRepository
        {
            get
            {
                return _positionRepository ??= new PositionRepository(_context);
            }
        }

        public IProductOnStorageEmployeesRepository ProductOnStorageEmployeesRepository
        {
            get
            {
                return _productOnStorageEmployeesRepository ??= new ProductOnStorageEmployeesRepository(_context);
            }
        }

        public IProductOnStorageRepository ProductOnStorageRepository
        {
            get
            {
                return _productOnStorageRepository ??= new ProductOnStorageRepository(_context);
            }
        }

        public IProductOrderItemRepository ProductOrderItemRepository
        {
            get
            {
                return _poductOrderItemRepository ??= new ProductOrderItemRepository(_context);
            }
        }

        public IProductOrdersRepository ProductOrdersRepository
        {
            get
            {
                return _productOrdersRepository ??= new ProductOrdersRepository(_context);
            }
        }

        public IProductsRepository ProductsRepository
        {
            get
            {
                return _productsRepository ??= new ProductsRepository(_context);
            }
        }

        public IRawStaffOnStorageRepository RawStaffOnStorageRepository
        {
            get
            {
                return _rawStaffOnStorageRepository ??= new RawStaffOnStorageRepository(_context);
            }
        }

        public IRawStaffOrderItemRepository RawStaffOrderItemRepository
        {
            get
            {
                return _rawStaffOrderItemRepository ??= new RawstaffOrderItemRepository(_context);
            }
        }

        public IRawStaffOrdersRepository RawStaffOrdersRepository
        {
            get
            {
                return _rawStaffOrdersRepository ??= new RawStaffOrdersRepository(_context);
            }
        }

        public IRawStaffRepository RawStaffRepository
        {
            get
            {
                return _rawStaffRepository ??= new RawStaffRepository(_context);
            }
        }

        public IRemovedProductsRepository RemovedProductsRepository
        {
            get
            {
                return _removedProductsRepository ??= new RemovedProductRepository(_context);
            }
        }

        public IRemovedRawStaffRepository RemovedRawStaffRepository
        {
            get
            {
                return _removedRawStaffRepository ??= new RemovedRawStaffRepository(_context);
            }
        }

        public IStoragesRepository StoragesRepository
        {
            get
            {
                return _storagesRepository ??= new StoragesRepository(_context);
            }
        }

        public ISuppliersRawStaffRepository SuppliersRawStaffRepository
        {
            get
            {
                return _suppliersRawStaffRepository ??= new SuppliersRawstaffRepository(_context);
            }
        }

        public ISuppliersRepository SuppliersRepository
        {
            get
            {
                return _suppliersRepository ??= new SuppliersRepository(_context);
            }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
