﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class ProductOnStorageRepository : BaseRepository<ProductOnStorage>, IProductOnStorageRepository
    {
        public ProductOnStorageRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.ProductOnStorage.Find(id);
            if (entity != null)
                _context.ProductOnStorage.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public ProductOnStorage GetById(int id)
        {
            var entity = _context.ProductOnStorage.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
