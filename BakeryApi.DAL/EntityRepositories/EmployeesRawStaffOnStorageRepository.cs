﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Linq;

namespace BakeryApi.DAL.EntityRepositories
{
    public class EmployeesRawStaffOnStorageRepository : BaseRepository<EmployeesRawStaffOnStorage>, IEmployeesRawStaffOnStorageRepository
    {
        public EmployeesRawStaffOnStorageRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int EmployeeId, int RawStaffID)
        {
            var entities = _context.EmployeesRawStaffOnStorage.Where(
                EmpRawStaff => EmpRawStaff.IdEmployee == EmployeeId && EmpRawStaff.IdRawStaffOnStorage == RawStaffID
                );
            if (entities != null)
                _context.EmployeesRawStaffOnStorage.RemoveRange(entities);
            else
                throw new Exception("Not Found");
        }

        public IQueryable<EmployeesRawStaffOnStorage> GetByEmployeerId(int id)
        {
            var entities = _context.EmployeesRawStaffOnStorage.Where(
                EmpProdOrd => EmpProdOrd.IdEmployee == id
                );
            if (entities != null)
                return entities;
            else
                throw new Exception("Not Found");
        }
    }
}
