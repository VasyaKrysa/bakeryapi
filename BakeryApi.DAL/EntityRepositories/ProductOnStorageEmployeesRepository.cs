﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class ProductOnStorageEmployeesRepository : BaseRepository<ProductOnStorageEmployees>, IProductOnStorageEmployeesRepository
    {
        public ProductOnStorageEmployeesRepository(BakeryContext context) : base(context)
        {
        }
        public void Delete(int EmployeeId, int ProductID)
        {
            var entities = _context.ProductOnStorageEmployees.Where(
                EmpProdOrd => EmpProdOrd.IdEmployee == EmployeeId && EmpProdOrd.IdProductOnStorage == ProductID
                );
            if (entities != null)
                _context.ProductOnStorageEmployees.RemoveRange(entities);
            else
                throw new Exception("Not Found");
        }

        public IQueryable<ProductOnStorageEmployees> GetByEmployeerId(int id)
        {
            var entities = _context.ProductOnStorageEmployees.Where(
                EmpProdOrd => EmpProdOrd.IdEmployee == id
                );
            if (entities != null)
                return entities;
            else
                throw new Exception("Not Found");
        }
    }
}
