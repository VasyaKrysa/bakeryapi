﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class StoragesRepository : BaseRepository<Storages>, IStoragesRepository
    {
        public StoragesRepository(BakeryContext context) : base(context)
        {
        }
        public void Delete(int id)
        {
            var entity = _context.Storages.Find(id);
            if (entity != null)
                _context.Storages.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public Storages GetById(int id)
        {
            var entity = _context.Storages.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
