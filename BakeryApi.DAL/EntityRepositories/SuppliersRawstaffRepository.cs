﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class SuppliersRawstaffRepository : BaseRepository<SuppliersRawStaff>, ISuppliersRawStaffRepository
    {
        public SuppliersRawstaffRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int SupplierId, int RawStaffID)
        {
            var entities = _context.SuppliersRawStaff.Where(
                Supp => Supp.IdSupplier == SupplierId && Supp.IdRawStaff == RawStaffID
                );
            if (entities != null)
                _context.SuppliersRawStaff.RemoveRange(entities);
            else
                throw new Exception("Not Found");
        }

        public IQueryable<SuppliersRawStaff> GetBySupplierId(int id)
        {
            var entities = _context.SuppliersRawStaff.Where(
                Supp => Supp.IdSupplier == id
                );
            if (entities != null)
                return entities;
            else
                throw new Exception("Not Found");
        }

        public void DeleteAll(int Suplierid)
        {
            var entities = _context.SuppliersRawStaff.Where(
              Supp => Supp.IdSupplier == Suplierid
              );
            if (entities != null)
                _context.SuppliersRawStaff.RemoveRange(entities);
            else
                throw new Exception("Not Found");
        }

    }
}
