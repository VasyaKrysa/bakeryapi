﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class RawStaffOrdersRepository : BaseRepository<RawStaffOrders>, IRawStaffOrdersRepository
    {
        public RawStaffOrdersRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.RawStaffOrders.Find(id);
            if (entity != null)
                _context.RawStaffOrders.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public RawStaffOrders GetById(int id)
        {
            var entity = _context.RawStaffOrders.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
