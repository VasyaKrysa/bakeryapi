﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class RawStaffOnStorageRepository : BaseRepository<RawStaffOnStorage>, IRawStaffOnStorageRepository
    {
        public RawStaffOnStorageRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.RawStaffOnStorage.Find(id);
            if (entity != null)
                _context.RawStaffOnStorage.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public RawStaffOnStorage GetById(int id)
        {
            var entity = _context.RawStaffOnStorage.Find(id);

            if (entity != null)
            {
                 _context.Entry(entity).Reference(p => p.IdRawStaffNavigation).Load();
                _context.Entry(entity).Reference(p => p.IdStorageNavigation).Load();
                _context.Entry(entity.IdRawStaffNavigation).Reference(p => p.IdMeasureNavigation).Load();
                _context.Entry(entity.IdStorageNavigation).Reference(p => p.IdAddressNavigation).Load();

                return entity;
            }
            else
                throw new Exception("Not found");
        }
    }
}
