﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class RemovedProductRepository : BaseRepository<RemovedProduct>, IRemovedProductsRepository
    {
        public RemovedProductRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.RemovedProduct.Find(id);
            if (entity != null)
                _context.RemovedProduct.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public RemovedProduct GetById(int id)
        {
            _context.RemovedProduct
               .Include(removedProducts => removedProducts.IdEmployeeNavigation)
               .Include(removedProducts => removedProducts.IdProductNavigation)
               .ToList();
            var entity = _context.RemovedProduct.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }

        public IQueryable<RemovedProduct> GetAll()
        {
            return _context.RemovedProduct
               .Include(removedProducts => removedProducts.IdEmployeeNavigation)
               .Include(removedProducts => removedProducts.IdProductNavigation);
        }
    }
}
