﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;

namespace BakeryApi.DAL.EntityRepositories
{
    public class ProductOrderItemRepository : BaseRepository<ProductOrderItems>, IProductOrderItemRepository
    {
        public ProductOrderItemRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.ProductOrderItems.Find(id);
            if (entity != null)
                _context.ProductOrderItems.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public ProductOrderItems GetById(int id)
        {
            var entity = _context.ProductOrderItems.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
