﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BakeryApi.DAL.EntityRepositories
{
    public class ProductOrdersRepository : BaseRepository<ProductOrders>, IProductOrdersRepository
    {
        public ProductOrdersRepository(BakeryContext context) : base(context)
        {
        }

        public IQueryable<ProductOrders> GetAll()
        {
            return _context.ProductOrders
               .Include(product=>product.IdCustomerNavigation)
                    .ThenInclude(cuss=>cuss.IdAddressNavigation)
               .Include(product=>product.IdOrderStatusNavigation);
        }

        public void Delete(int id)
        {
            var entity = _context.ProductOrders.Find(id);
            if (entity != null)
                _context.ProductOrders.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public ProductOrders GetById(int id)
        {
            _context.ProductOrders
               .Include(product => product.IdCustomerNavigation)
                    .ThenInclude(cuss => cuss.IdAddressNavigation)
               .Include(product => product.IdOrderStatusNavigation)
               .ToList();

            var entity = _context.ProductOrders.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
