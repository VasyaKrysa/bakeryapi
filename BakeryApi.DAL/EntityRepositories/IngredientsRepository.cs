﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class IngredientsRepository : BaseRepository<Ingredients>, IIngredientsRepository
    {
        public IngredientsRepository(BakeryContext context) : base(context)
        {
        }
        public void Delete(int id)
        {
            var entity = _context.Ingredients.Find(id);
            if (entity != null)
                _context.Ingredients.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public Ingredients GetById(int id)
        {
            var entity = _context.Ingredients.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
