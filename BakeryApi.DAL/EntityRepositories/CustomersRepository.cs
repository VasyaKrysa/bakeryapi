﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BakeryApi.DAL.EntityRepositories
{
    public class CustomersRepository : BaseRepository<Customers>, ICustomersRepository
    {
        public CustomersRepository(BakeryContext context) : base(context)
        {
        }

        public IQueryable<Customers> GetAll()
        {
            return _context.Customers
               .Include(customer => customer.IdAddressNavigation);
        }

        public void Delete(int id)
        {
            var entity = _context.Customers.Find(id);
            if (entity != null)
                _context.Customers.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public Customers GetById(int id)
        {
            _context.Customers
               .Include(customer => customer.IdAddressNavigation)
               .ToList();
            var entity = _context.Customers.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
