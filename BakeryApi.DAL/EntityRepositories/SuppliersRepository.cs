﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class SuppliersRepository : BaseRepository<Suppliers>, ISuppliersRepository
    {
        public SuppliersRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.Suppliers.Find(id);
            var entities = _context.SuppliersRawStaff.Where(e => e.IdSupplier==id);
            if (entity != null) 
            {
                _context.Suppliers.Remove(entity);
                if (entities != null)
                    _context.SuppliersRawStaff.RemoveRange(entities);
            }
            else
                throw new Exception("Not found");
        }

        public Suppliers GetById(int id)
        {
            var entity = _context.Suppliers.Find(id);
            if (entity != null)
            {
                _context.Entry(entity).Reference(e => e.IdAddressNavigation).Load();
                return entity;
            }
            else
                throw new Exception("Not found");
        }

        public void AddRawStaffToSupplier(int supplierID, int rawStaffID)
        {
            var supplier = _context.Suppliers.Find(supplierID);
            if (supplier == null)
                throw new Exception("Supplier not found");

            var rawStaff = _context.RawStaff.Find(rawStaffID);
            if (rawStaff == null)
                throw new Exception("Raw Staff not found");

            var supplierRawStaff = new SuppliersRawStaff();
            supplierRawStaff.IdSupplier = supplierID;
            supplierRawStaff.IdRawStaff = rawStaffID;
            _context.SuppliersRawStaff.Add(supplierRawStaff);
        }

        public IQueryable<RawStaff> GetAllRawStaff(int supplierID)
        {
            var rawStaffSuppliers = _context.SuppliersRawStaff.Where(s => s.IdSupplier == supplierID).ToList();
            List<RawStaff> rawStaff = new List<RawStaff>();
            foreach (var rawStaffSupp in rawStaffSuppliers)
            {
                var rawStaffID = rawStaffSupp.IdRawStaff;
                rawStaff.Add(_context.RawStaff.Find(rawStaffID));
            }
            return rawStaff.AsQueryable();
        }

    }
}
