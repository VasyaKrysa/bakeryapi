﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class OrderStatusesRepository : BaseRepository<OrderStatuses>, IOrderStatusesRepository
    {
        public OrderStatusesRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.OrderStatuses.Find(id);
            if (entity != null)
                _context.OrderStatuses.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public OrderStatuses GetById(int id)
        {
            var entity = _context.OrderStatuses.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
