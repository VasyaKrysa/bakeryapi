﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class MeasureRepository : BaseRepository<Measure>, IMeasureRepository
    {
        public MeasureRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.Measure.Find(id);
            if (entity != null)
                _context.Measure.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public Measure GetById(int? id)
        {
            if (id == null)
                throw new Exception("Null exception");
            var entity = _context.Measure.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
