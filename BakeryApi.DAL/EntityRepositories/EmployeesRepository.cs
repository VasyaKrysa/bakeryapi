﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BakeryApi.DAL.EntityRepositories
{
    public class EmployeesRepository : BaseRepository<Employees>, IEmployeesRepository
    {
        public EmployeesRepository(BakeryContext context) : base(context)
        {
        }

        public IQueryable<Employees> GetAll()
        {
           return _context.Employees
               .Include(employee => employee.IdAddressNavigation)
               .Include(employee=> employee.IdPositionNavigation);
        }
        public void Delete(int id)
        {
            var entity = _context.Employees.Find(id);
            if (entity != null)
                _context.Employees.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public Employees GetById(int id)
        {
            _context.Employees
               .Include(employee => employee.IdAddressNavigation)
               .Include(employee => employee.IdPositionNavigation)
               .ToList();

            var entity = _context.Employees.Find(id);
            if (entity != null)
                return entity;
            else
                throw new Exception("Not found");
        }
    }
}
