﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class RemovedRawStaffRepository : BaseRepository<RemovedRowStaff>, IRemovedRawStaffRepository
    {
        public RemovedRawStaffRepository(BakeryContext context) : base(context)
        {
        }
        public void Delete(int id)
        {
            var entity = _context.RemovedRowStaff.Find(id);
            if (entity != null)
                _context.RemovedRowStaff.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public RemovedRowStaff GetById(int id)
        {
            var entity = _context.RemovedRowStaff.Find(id);
            

            if (entity != null)
            {
                _context.Entry(entity).Reference(p => p.IdRawStaffNavigation).Load();
                _context.Entry(entity).Reference(p => p.IdEmployeeNavigation).Load();
                _context.Entry(entity.IdRawStaffNavigation).Reference(p => p.IdMeasureNavigation).Load();
                _context.Entry(entity.IdEmployeeNavigation).Reference(p => p.IdAddressNavigation).Load();
                _context.Entry(entity.IdEmployeeNavigation).Reference(p => p.IdPositionNavigation).Load();

                return entity;
            }     
            else
                throw new Exception("Not found");
        }
    }
}
