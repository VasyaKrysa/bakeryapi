﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class EmployeesProductOrderRepository:BaseRepository<EmployeesProductOrders>,IEmployeesProductOrdersRepository
    {
        public EmployeesProductOrderRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int EmployeeId, int OrderID)
        {
            var entities=_context.EmployeesProductOrders.Where(
                EmpProdOrd => EmpProdOrd.IdEmployee == EmployeeId && EmpProdOrd.IdOrder == OrderID
                );
            if (entities != null)
                _context.EmployeesProductOrders.RemoveRange(entities);
            else
                throw new Exception("Not Found");
        }

        public IQueryable<EmployeesProductOrders> GetByEmployeerId(int id)
        {
            var entities = _context.EmployeesProductOrders.Where(
                EmpProdOrd => EmpProdOrd.IdEmployee == id
                );
            if (entities != null)
                return entities;
            else
                throw new Exception("Not Found");
        }
    }
}
