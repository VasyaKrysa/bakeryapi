﻿using BakeryApi.Core.Abstractions.Repositories;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using BakeryApi.DAL.BaseRepository;
using BakeryApi.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.DAL.EntityRepositories
{
    public class RawStaffRepository : BaseRepository<RawStaff>, IRawStaffRepository
    {
        public RawStaffRepository(BakeryContext context) : base(context)
        {
        }

        public void Delete(int id)
        {
            var entity = _context.RawStaff.Find(id);
            if (entity != null)
                _context.RawStaff.Remove(entity);
            else
                throw new Exception("Not found");
        }

        public RawStaff GetById(int id)
        {
            var entity = _context.RawStaff.Find(id);
            if (entity != null)
            {
                _context.Entry(entity).Reference(e => e.IdMeasureNavigation).Load();
                return entity;
            }
            else
                throw new Exception("Not found");
        }

        public IQueryable<RawStaff> GetByName(string name)
        {
            return  _context.RawStaff
                    .Where(rawStaff => EF.Functions.Like(rawStaff.Name, name + "%"))
                    .Take(10).AsQueryable();        
        }
    }
}
