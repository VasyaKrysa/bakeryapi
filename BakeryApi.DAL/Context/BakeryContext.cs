﻿using System;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BakeryApi.DAL.Context
{
    public partial class BakeryContext : DbContext
    {
        public BakeryContext()
        {
        }

        public BakeryContext(DbContextOptions<BakeryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<EmployeesProductOrders> EmployeesProductOrders { get; set; }
        public virtual DbSet<EmployeesRawStaffOnStorage> EmployeesRawStaffOnStorage { get; set; }
        public virtual DbSet<EmployeesRawStaffOrders> EmployeesRawStaffOrders { get; set; }
        public virtual DbSet<Ingredients> Ingredients { get; set; }
        public virtual DbSet<Measure> Measure { get; set; }
        public virtual DbSet<OrderStatuses> OrderStatuses { get; set; }
        public virtual DbSet<Positions> Positions { get; set; }
        public virtual DbSet<ProductOnStorage> ProductOnStorage { get; set; }
        public virtual DbSet<ProductOnStorageEmployees> ProductOnStorageEmployees { get; set; }
        public virtual DbSet<ProductOrderItems> ProductOrderItems { get; set; }
        public virtual DbSet<ProductOrders> ProductOrders { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<RawStaff> RawStaff { get; set; }
        public virtual DbSet<RawStaffOnStorage> RawStaffOnStorage { get; set; }
        public virtual DbSet<RawStaffOrderItems> RawStaffOrderItems { get; set; }
        public virtual DbSet<RawStaffOrders> RawStaffOrders { get; set; }
        public virtual DbSet<RemovedProduct> RemovedProduct { get; set; }
        public virtual DbSet<RemovedRowStaff> RemovedRowStaff { get; set; }
        public virtual DbSet<Storages> Storages { get; set; }
        public virtual DbSet<Suppliers> Suppliers { get; set; }
        public virtual DbSet<SuppliersRawStaff> SuppliersRawStaff { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=IGOR-PC\\MSSQLSERVER01;Database=Bakery;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.HasKey(e => e.IdAddress)
                    .HasName("XPKAddress");

                entity.Property(e => e.IdAddress).HasColumnName("ID_Address");

                entity.Property(e => e.City).HasMaxLength(20);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.HouseNumber).HasColumnName("House_Number");

                entity.Property(e => e.Street).HasMaxLength(20);
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.HasKey(e => e.IdCustomer)
                    .HasName("XPKCustomers");

                entity.Property(e => e.IdCustomer).HasColumnName("ID_Customer");

                entity.Property(e => e.IdAddress).HasColumnName("ID_Address");

                entity.Property(e => e.Name).HasMaxLength(20);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("Phone_Number")
                    .HasMaxLength(13);

                entity.HasOne(d => d.IdAddressNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.IdAddress)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_55");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.IdEmployee)
                    .HasName("XPKEmployees");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.Property(e => e.Experience).HasMaxLength(20);

                entity.Property(e => e.IdAddress).HasColumnName("ID_Address");

                entity.Property(e => e.IdPosition).HasColumnName("ID_Position");

                entity.Property(e => e.Name).HasMaxLength(20);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("Phone_Number")
                    .HasMaxLength(13);

                entity.Property(e => e.Surname).HasMaxLength(20);

                entity.HasOne(d => d.IdAddressNavigation)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.IdAddress)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_42");

                entity.HasOne(d => d.IdPositionNavigation)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.IdPosition)
                    .HasConstraintName("R_43");
            });

            modelBuilder.Entity<EmployeesProductOrders>(entity =>
            {
                entity.HasKey(e => new { e.IdEmployee, e.IdOrder })
                    .HasName("XPKEmployees_Product_Orders");

                entity.ToTable("Employees_Product_Orders");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.Property(e => e.IdOrder).HasColumnName("ID_Order");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.EmployeesProductOrders)
                    .HasForeignKey(d => d.IdEmployee)
                    .HasConstraintName("R_60");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany(p => p.EmployeesProductOrders)
                    .HasForeignKey(d => d.IdOrder)
                    .HasConstraintName("R_32");
            });

            modelBuilder.Entity<EmployeesRawStaffOnStorage>(entity =>
            {
                entity.HasKey(e => new { e.IdEmployee, e.IdRawStaffOnStorage })
                    .HasName("XPKEmployees_Raw_Staff_On_Storage");

                entity.ToTable("Employees_Raw_Staff_On_Storage");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.Property(e => e.IdRawStaffOnStorage).HasColumnName("ID_Raw_Staff_On_Storage");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.EmployeesRawStaffOnStorage)
                    .HasForeignKey(d => d.IdEmployee)
                    .HasConstraintName("R_101");

                entity.HasOne(d => d.IdRawStaffOnStorageNavigation)
                    .WithMany(p => p.EmployeesRawStaffOnStorage)
                    .HasForeignKey(d => d.IdRawStaffOnStorage)
                    .HasConstraintName("R_34");
            });

            modelBuilder.Entity<EmployeesRawStaffOrders>(entity =>
            {
                entity.HasKey(e => new { e.IdEmployee, e.IdOrder })
                    .HasName("XPKEmployees_Raw_Staff_Orders");

                entity.ToTable("Employees_Raw_Staff_Orders");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.Property(e => e.IdOrder).HasColumnName("ID_Order");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.EmployeesRawStaffOrders)
                    .HasForeignKey(d => d.IdEmployee)
                    .HasConstraintName("R_201");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany(p => p.EmployeesRawStaffOrders)
                    .HasForeignKey(d => d.IdOrder)
                    .HasConstraintName("R_202");
            });

            modelBuilder.Entity<Ingredients>(entity =>
            {
                entity.HasKey(e => e.IdIngredient)
                    .HasName("XPKIngredients");

                entity.Property(e => e.IdIngredient).HasColumnName("ID_Ingredient");

                entity.Property(e => e.IdMeasure).HasColumnName("ID_Measure");

                entity.Property(e => e.IdProduct).HasColumnName("ID_Product");

                entity.Property(e => e.IdRawStaff).HasColumnName("ID_Raw_Staff");

                entity.Property(e => e.Weight).HasColumnType("decimal(8, 3)");

                entity.HasOne(d => d.IdMeasureNavigation)
                    .WithMany(p => p.Ingredients)
                    .HasForeignKey(d => d.IdMeasure)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_79");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.Ingredients)
                    .HasForeignKey(d => d.IdProduct)
                    .HasConstraintName("R_97");

                entity.HasOne(d => d.IdRawStaffNavigation)
                    .WithMany(p => p.Ingredients)
                    .HasForeignKey(d => d.IdRawStaff)
                    .HasConstraintName("R_98");
            });

            modelBuilder.Entity<Measure>(entity =>
            {
                entity.HasKey(e => e.IdMeasure)
                    .HasName("XPKMeasure");

                entity.Property(e => e.IdMeasure).HasColumnName("ID_Measure");

                entity.Property(e => e.Name).HasMaxLength(20);

                entity.Property(e => e.PartOfKg).HasColumnType("decimal(8, 4)");
            });

            modelBuilder.Entity<OrderStatuses>(entity =>
            {
                entity.HasKey(e => e.IdOrderStatus)
                    .HasName("XPKOrder_Statuses");

                entity.ToTable("Order_Statuses");

                entity.Property(e => e.IdOrderStatus).HasColumnName("ID_Order_Status");

                entity.Property(e => e.Status).HasMaxLength(30);
            });

            modelBuilder.Entity<Positions>(entity =>
            {
                entity.HasKey(e => e.IdPosition)
                    .HasName("XPKPositions");

                entity.Property(e => e.IdPosition).HasColumnName("ID_Position");

                entity.Property(e => e.PositionName)
                    .HasColumnName("Position_Name")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<ProductOnStorage>(entity =>
            {
                entity.HasKey(e => e.IdProductOnStorage)
                    .HasName("XPKProduct_On_Storage");

                entity.ToTable("Product_On_Storage");

                entity.Property(e => e.IdProductOnStorage).HasColumnName("ID_Product_On_Storage");

                entity.Property(e => e.DateOfManufacture)
                    .HasColumnName("Date_Of_Manufacture")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdProduct).HasColumnName("ID_Product");

                entity.Property(e => e.IdStorage).HasColumnName("ID_Storage");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.ProductOnStorage)
                    .HasForeignKey(d => d.IdProduct)
                    .HasConstraintName("R_61");

                entity.HasOne(d => d.IdStorageNavigation)
                    .WithMany(p => p.ProductOnStorage)
                    .HasForeignKey(d => d.IdStorage)
                    .HasConstraintName("R_50");
            });

            modelBuilder.Entity<ProductOnStorageEmployees>(entity =>
            {
                entity.HasKey(e => new { e.IdProductOnStorage, e.IdEmployee })
                    .HasName("XPKProduct_On_Storage_Employees");

                entity.ToTable("Product_On_Storage_Employees");

                entity.Property(e => e.IdProductOnStorage).HasColumnName("ID_Product_On_Storage");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.ProductOnStorageEmployees)
                    .HasForeignKey(d => d.IdEmployee)
                    .HasConstraintName("R_36");

                entity.HasOne(d => d.IdProductOnStorageNavigation)
                    .WithMany(p => p.ProductOnStorageEmployees)
                    .HasForeignKey(d => d.IdProductOnStorage)
                    .HasConstraintName("R_71");
            });

            modelBuilder.Entity<ProductOrderItems>(entity =>
            {
                entity.HasKey(e => e.IdProductOrderItem)
                    .HasName("XPKProduct_Order_Items");

                entity.ToTable("Product_Order_Items");

                entity.Property(e => e.IdProductOrderItem).HasColumnName("ID_Product_Order_Item");

                entity.Property(e => e.IdOrder).HasColumnName("ID_Order");

                entity.Property(e => e.IdProduct).HasColumnName("ID_Product");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany(p => p.ProductOrderItems)
                    .HasForeignKey(d => d.IdOrder)
                    .HasConstraintName("R_83");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.ProductOrderItems)
                    .HasForeignKey(d => d.IdProduct)
                    .HasConstraintName("R_81");
            });

            modelBuilder.Entity<ProductOrders>(entity =>
            {
                entity.HasKey(e => e.IdOrder)
                    .HasName("XPKProduct_Orders");

                entity.ToTable("Product_Orders");

                entity.Property(e => e.IdOrder).HasColumnName("ID_Order");

                entity.Property(e => e.IdCustomer).HasColumnName("ID_Customer");

                entity.Property(e => e.IdOrderStatus).HasColumnName("ID_Order_Status");

                entity.Property(e => e.Time).HasColumnType("datetime");

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("Total_Price")
                    .HasColumnType("decimal(8, 2)");

                entity.HasOne(d => d.IdCustomerNavigation)
                    .WithMany(p => p.ProductOrders)
                    .HasForeignKey(d => d.IdCustomer)
                    .HasConstraintName("R_56");

                entity.HasOne(d => d.IdOrderStatusNavigation)
                    .WithMany(p => p.ProductOrders)
                    .HasForeignKey(d => d.IdOrderStatus)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_54");
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.HasKey(e => e.IdProduct)
                    .HasName("XPKProducts");

                entity.Property(e => e.IdProduct).HasColumnName("ID_Product");

                entity.Property(e => e.Desctiption).HasMaxLength(20);

                entity.Property(e => e.ExpirationTerm).HasColumnName("Expiration_term");

                entity.Property(e => e.IdMeasure).HasColumnName("ID_Measure");

                entity.Property(e => e.MarkUp)
                    .HasColumnName("Mark_up")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Price).HasColumnType("decimal(6, 2)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("Product_name")
                    .HasMaxLength(20);

                entity.Property(e => e.Weight).HasColumnType("decimal(8, 2)");

                entity.HasOne(d => d.IdMeasureNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.IdMeasure)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_78");
            });

            modelBuilder.Entity<RawStaff>(entity =>
            {
                entity.HasKey(e => e.IdRawStaff)
                    .HasName("XPKRaw_Staff");

                entity.ToTable("Raw_Staff");

                entity.Property(e => e.IdRawStaff).HasColumnName("ID_Raw_Staff");

                entity.Property(e => e.ExpirationDate).HasColumnName("Expiration_Date");

                entity.Property(e => e.IdMeasure).HasColumnName("ID_Measure");

                entity.Property(e => e.Name).HasMaxLength(20);

                entity.Property(e => e.PricePerMeasure)
                    .HasColumnName("Price_Per_Measure")
                    .HasColumnType("decimal(6, 2)");

                entity.HasOne(d => d.IdMeasureNavigation)
                    .WithMany(p => p.RawStaff)
                    .HasForeignKey(d => d.IdMeasure)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_89");
            });

            modelBuilder.Entity<RawStaffOnStorage>(entity =>
            {
                entity.HasKey(e => e.IdRawStaffOnStorage)
                    .HasName("XPKRaw_Staff_On_Storage");

                entity.ToTable("Raw_Staff_On_Storage");

                entity.Property(e => e.IdRawStaffOnStorage).HasColumnName("ID_Raw_Staff_On_Storage");

                entity.Property(e => e.Amount).HasColumnType("decimal(8, 3)");

                entity.Property(e => e.ConsumeUntil)
                    .HasColumnName("Consume_Until")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdRawStaff).HasColumnName("ID_Raw_Staff");

                entity.Property(e => e.IdStorage).HasColumnName("ID_Storage");

                entity.HasOne(d => d.IdRawStaffNavigation)
                    .WithMany(p => p.RawStaffOnStorage)
                    .HasForeignKey(d => d.IdRawStaff)
                    .HasConstraintName("R_87");

                entity.HasOne(d => d.IdStorageNavigation)
                    .WithMany(p => p.RawStaffOnStorage)
                    .HasForeignKey(d => d.IdStorage)
                    .HasConstraintName("R_51");
            });

            modelBuilder.Entity<RawStaffOrderItems>(entity =>
            {
                entity.HasKey(e => e.IdRawStaffOrderItem)
                    .HasName("XPKRaw_Staff_Order_Items");

                entity.ToTable("Raw_Staff_Order_Items");

                entity.Property(e => e.IdRawStaffOrderItem).HasColumnName("ID_Raw_Staff_Order_Item");

                entity.Property(e => e.Amount).HasColumnType("decimal(8, 3)");

                entity.Property(e => e.IdOrder).HasColumnName("ID_Order");

                entity.Property(e => e.IdRawStaff).HasColumnName("ID_Raw_Staff");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany(p => p.RawStaffOrderItems)
                    .HasForeignKey(d => d.IdOrder)
                    .HasConstraintName("R_94");

                entity.HasOne(d => d.IdRawStaffNavigation)
                    .WithMany(p => p.RawStaffOrderItems)
                    .HasForeignKey(d => d.IdRawStaff)
                    .HasConstraintName("R_92");
            });

            modelBuilder.Entity<RawStaffOrders>(entity =>
            {
                entity.HasKey(e => e.IdOrder)
                    .HasName("XPKRaw_Staff_Orders");

                entity.ToTable("Raw_Staff_Orders");

                entity.Property(e => e.IdOrder).HasColumnName("ID_Order");

                entity.Property(e => e.IdOrderStatus).HasColumnName("ID_Order_Status");

                entity.Property(e => e.IdSupplier).HasColumnName("ID_Supplier");

                entity.Property(e => e.Time).HasColumnType("datetime");

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("Total_Price")
                    .HasColumnType("decimal(8, 2)");

                entity.HasOne(d => d.IdOrderStatusNavigation)
                    .WithMany(p => p.RawStaffOrders)
                    .HasForeignKey(d => d.IdOrderStatus)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_53");

                entity.HasOne(d => d.IdSupplierNavigation)
                    .WithMany(p => p.RawStaffOrders)
                    .HasForeignKey(d => d.IdSupplier)
                    .HasConstraintName("R_123");
            });

            modelBuilder.Entity<RemovedProduct>(entity =>
            {
                entity.HasKey(e => e.IdRemovedProduct)
                    .HasName("XPKRemoved_Product");

                entity.ToTable("Removed_Product");

                entity.Property(e => e.IdRemovedProduct).HasColumnName("ID_Removed_Product");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.Property(e => e.IdProduct).HasColumnName("ID_Product");

                entity.Property(e => e.ReasonToRemove)
                    .HasColumnName("Reason_To_Remove")
                    .HasMaxLength(20);

                entity.Property(e => e.RemovingDate)
                    .HasColumnName("Removing_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.RemovedProduct)
                    .HasForeignKey(d => d.IdEmployee)
                    .HasConstraintName("R_58");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.RemovedProduct)
                    .HasForeignKey(d => d.IdProduct)
                    .HasConstraintName("R_99");
            });

            modelBuilder.Entity<RemovedRowStaff>(entity =>
            {
                entity.HasKey(e => e.IdRemovedRowStaff)
                    .HasName("XPKRemoved_Row_Staff");

                entity.ToTable("Removed_Row_Staff");

                entity.Property(e => e.IdRemovedRowStaff).HasColumnName("ID_Removed_Row_Staff");

                entity.Property(e => e.Amount).HasColumnType("decimal(8, 3)");

                entity.Property(e => e.IdEmployee).HasColumnName("ID_Employee");

                entity.Property(e => e.IdRawStaff).HasColumnName("ID_Raw_Staff");

                entity.Property(e => e.ReasonToRemove)
                    .HasColumnName("Reason_To_Remove")
                    .HasMaxLength(20);

                entity.Property(e => e.RemovingDate)
                    .HasColumnName("Removing_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.RemovedRowStaff)
                    .HasForeignKey(d => d.IdEmployee)
                    .HasConstraintName("R_44");

                entity.HasOne(d => d.IdRawStaffNavigation)
                    .WithMany(p => p.RemovedRowStaff)
                    .HasForeignKey(d => d.IdRawStaff)
                    .HasConstraintName("R_100");
            });

            modelBuilder.Entity<Storages>(entity =>
            {
                entity.HasKey(e => e.IdStorage)
                    .HasName("XPKStorages");

                entity.Property(e => e.IdStorage).HasColumnName("ID_Storage");

                entity.Property(e => e.IdAddress).HasColumnName("ID_Address");

                entity.HasOne(d => d.IdAddressNavigation)
                    .WithMany(p => p.Storages)
                    .HasForeignKey(d => d.IdAddress)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_49");
            });

            modelBuilder.Entity<Suppliers>(entity =>
            {
                entity.HasKey(e => e.IdSupplier)
                    .HasName("XPKSuppliers");

                entity.Property(e => e.IdSupplier).HasColumnName("ID_Supplier");

                entity.Property(e => e.IdAddress).HasColumnName("ID_Address");

                entity.Property(e => e.Name).HasMaxLength(20);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("Phone_number")
                    .HasMaxLength(13);

                entity.HasOne(d => d.IdAddressNavigation)
                    .WithMany(p => p.Suppliers)
                    .HasForeignKey(d => d.IdAddress)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("R_33");
            });

            modelBuilder.Entity<SuppliersRawStaff>(entity =>
            {
                entity.HasKey(e => new { e.IdSupplier, e.IdRawStaff })
                    .HasName("XPKSuppliers_Raw_Staff");

                entity.ToTable("Suppliers_Raw_Staff");

                entity.Property(e => e.IdSupplier).HasColumnName("ID_Supplier");

                entity.Property(e => e.IdRawStaff).HasColumnName("ID_Raw_Staff");

                entity.HasOne(d => d.IdRawStaffNavigation)
                    .WithMany(p => p.SuppliersRawStaff)
                    .HasForeignKey(d => d.IdRawStaff)
                    .HasConstraintName("R_38");

                entity.HasOne(d => d.IdSupplierNavigation)
                    .WithMany(p => p.SuppliersRawStaff)
                    .HasForeignKey(d => d.IdSupplier)
                    .HasConstraintName("R_93");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
