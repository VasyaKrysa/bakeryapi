﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class RawStaffOrderDTO
    {
        public int IdOrder { get; set; }
        public DateTime? Time { get; set; }
        public decimal? TotalPrice { get; set; }
        public int IdSupplier { get; set; }
        public SupplierDTO Supplier { get; set; }
        public int IdOrderStatus { get; set; }
        public string Status { get; set; }
    }
}
