﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class PositionDTO
    {
        public int IdPosition { get; set; }
        public string PositionName { get; set; }
        public int? Salary { get; set; }
    }
}
