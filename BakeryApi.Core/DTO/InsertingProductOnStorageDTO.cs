﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class InsertingProductOnStorageDTO
    {
        public ProductOnStorageDTO product { get; set; }
        public List<int> employeesId { get; set; }
    }
}
