﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class RawStaffOrderItemDTO
    {
        public int IdRawStaffOrderItem { get; set; }
        public int IdRawStaff { get; set; }
        public RawStaffDTO RawStaff { get; set; }
        public decimal? Amount { get; set; }
        public int IdOrder { get; set; }
        public RawStaffOrderDTO Order { get; set; }
    }
}
