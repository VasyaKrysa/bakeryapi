﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class StorageDTO
    {
        public int IdStorage { get; set; }
        public int IdAddress { get; set; }
        public AddressDTO Address { get; set; } 
    }
}
