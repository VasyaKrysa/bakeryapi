﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class EmployeeDTO
    {
        public int IdEmployee { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Experience { get; set; }
        public int IdAddress { get; set; }
        public int IdPosition { get; set; }

        public  AddressDTO Address { get; set; }
        public  PositionDTO Position { get; set; }
    }
}
