﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class RawStaffDTO
    {
        public int IdRawStaff { get; set; }
        public string Name { get; set; }
        public int? ExpirationDate { get; set; }
        public decimal? PricePerMeasure { get; set; }
        public int IdMeasure { get; set; }
        public string MeasureName { get; set; }
    }
}
