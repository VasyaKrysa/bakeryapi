﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class RawStaffOnStorageDTO
    {
        public int IdRawStaffOnStorage { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? ConsumeUntil { get; set; }
        public int IdStorage { get; set; }
        public int IdRawStaff { get; set; }
        public AddressDTO StorageAddress { get; set; }
        public RawStaffDTO RawStaff { get; set; }
    }
}
