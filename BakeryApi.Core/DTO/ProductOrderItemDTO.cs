﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class ProductOrderItemDTO
    {
        public int IdProductOrderItem { get; set; }
        public int IdProduct { get; set; }
        public ProductDTO Product { get; set; }
        public int? Amount { get; set; }
        public int IdOrder { get; set; }
        public ProductsOrderDTO Order { get; set; }
    }
}
