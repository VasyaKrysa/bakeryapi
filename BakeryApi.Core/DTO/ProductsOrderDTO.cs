﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class ProductsOrderDTO
    {
        public int IdOrder { get; set; }
        public DateTime? Time { get; set; }
        public decimal? TotalPrice { get; set; }
        public int IdOrderStatus { get; set; }
        public string Status { get; set; }
        public int IdCustomer { get; set; }
        public CustomerDTO Customer { get; set; }
    }
}
