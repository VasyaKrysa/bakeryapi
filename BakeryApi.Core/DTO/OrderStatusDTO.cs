﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class OrderStatusDTO
    {
        public int IdOrderStatus { get; set; }
        public string Status { get; set; }
    }
}
