﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class RemovedProductDTO
    {
        public int IdRemovedProduct { get; set; }
        public string ReasonToRemove { get; set; }
        public DateTime? RemovingDate { get; set; }
        public int IdEmployee { get; set; }
        public int? Amount { get; set; }
        public int IdProduct { get; set; }

        public  EmployeeDTO Employee { get; set; }
        public  ProductDTO Product { get; set; }
    }
}
