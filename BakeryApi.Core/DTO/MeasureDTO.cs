﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class MeasureDTO
    {
        public int IdMeasure { get; set; }
        public string Name { get; set; }
        public decimal? PartOfKg { get; set; }
    }
}
