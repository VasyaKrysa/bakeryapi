﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class ProductOnStorageDTO
    {
        public int IdProductOnStorage { get; set; }
        public int IdProduct { get; set; }
        public ProductDTO Product { get; set; }
        public int IdStorage { get; set; }
        public AddressDTO StorageAddress { get; set; }
        public int? Amount { get; set; }
        public DateTime? DateOfManufacture { get; set; }
    }
}
