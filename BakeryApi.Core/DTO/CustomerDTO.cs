﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class CustomerDTO
    {
        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int IdAddress { get; set; }

        public AddressDTO Address { get; set; }
    }
}
