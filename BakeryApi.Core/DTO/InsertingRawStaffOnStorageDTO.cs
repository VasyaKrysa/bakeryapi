﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class InsertingRawStaffOnStorageDTO
    {
        public RawStaffOnStorageDTO rawStaffOnStorage { get; set; }
        public List<int> employeesId { get; set; }
    }
}
