﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class IngredientDTO
    {
        public int IdIngredient { get; set; }

        public decimal? Weight { get; set; }

        public int IdMeasure { get; set; }

        public string MeasureName { get; set; }

        public int IdProduct { get; set; }

        public string ProductName { get; set; }

        public int IdRawStaff { get; set; }

        public string RawStaffName { get; set; }
    }
}
