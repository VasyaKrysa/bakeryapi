﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class RemovedRawStaffDTO
    {
        public int IdRemovedRowStaff { get; set; }
        public DateTime? RemovingDate { get; set; }
        public string ReasonToRemove { get; set; }
        public decimal? Amount { get; set; }
        public int IdRawStaff { get; set; }
        public RawStaffDTO RawStaff { get; set; }
        public int IdEmployee { get; set; }
        public EmployeeDTO Employee { get; set; }
    }
}
