﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.DTO
{
    public class ProductDTO
    {
        public int IdProduct { get; set; }
        public string ProductName { get; set; }
        public decimal? Weight { get; set; }
        public string Desctiption { get; set; }
        public int? ExpirationTerm { get; set; }
        public decimal? Price { get; set; }
        public decimal? MarkUp { get; set; }
        public int IdMeasure { get; set; }
        public string MeasureName { get; set; }
    }
}
