﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class RawStaffOnStorage
    {
        public RawStaffOnStorage()
        {
            EmployeesRawStaffOnStorage = new HashSet<EmployeesRawStaffOnStorage>();
        }

        public int IdRawStaffOnStorage { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? ConsumeUntil { get; set; }
        public int IdStorage { get; set; }
        public int IdRawStaff { get; set; }

        public virtual RawStaff IdRawStaffNavigation { get; set; }
        public virtual Storages IdStorageNavigation { get; set; }
        public virtual ICollection<EmployeesRawStaffOnStorage> EmployeesRawStaffOnStorage { get; set; }
    }
}
