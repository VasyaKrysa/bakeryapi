﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Address
    {
        public Address()
        {
            Customers = new HashSet<Customers>();
            Employees = new HashSet<Employees>();
            Storages = new HashSet<Storages>();
            Suppliers = new HashSet<Suppliers>();
        }

        public int IdAddress { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int? HouseNumber { get; set; }

        public virtual ICollection<Customers> Customers { get; set; }
        public virtual ICollection<Employees> Employees { get; set; }
        public virtual ICollection<Storages> Storages { get; set; }
        public virtual ICollection<Suppliers> Suppliers { get; set; }
    }
}
