﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class RawStaffOrders
    {
        public RawStaffOrders()
        {
            EmployeesRawStaffOrders = new HashSet<EmployeesRawStaffOrders>();
            RawStaffOrderItems = new HashSet<RawStaffOrderItems>();
        }

        public int IdOrder { get; set; }
        public DateTime? Time { get; set; }
        public decimal? TotalPrice { get; set; }
        public int IdSupplier { get; set; }
        public int? IdOrderStatus { get; set; }

        public virtual OrderStatuses IdOrderStatusNavigation { get; set; }
        public virtual Suppliers IdSupplierNavigation { get; set; }
        public virtual ICollection<EmployeesRawStaffOrders> EmployeesRawStaffOrders { get; set; }
        public virtual ICollection<RawStaffOrderItems> RawStaffOrderItems { get; set; }
    }
}
