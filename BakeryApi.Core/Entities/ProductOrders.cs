﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class ProductOrders
    {
        public ProductOrders()
        {
            EmployeesProductOrders = new HashSet<EmployeesProductOrders>();
            ProductOrderItems = new HashSet<ProductOrderItems>();
        }

        public int IdOrder { get; set; }
        public DateTime? Time { get; set; }
        public decimal? TotalPrice { get; set; }
        public int? IdOrderStatus { get; set; }
        public int IdCustomer { get; set; }

        public virtual Customers IdCustomerNavigation { get; set; }
        public virtual OrderStatuses IdOrderStatusNavigation { get; set; }
        public virtual ICollection<EmployeesProductOrders> EmployeesProductOrders { get; set; }
        public virtual ICollection<ProductOrderItems> ProductOrderItems { get; set; }
    }
}
