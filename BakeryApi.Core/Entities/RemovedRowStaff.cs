﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class RemovedRowStaff
    {
        public int IdRemovedRowStaff { get; set; }
        public DateTime? RemovingDate { get; set; }
        public string ReasonToRemove { get; set; }
        public decimal? Amount { get; set; }
        public int IdRawStaff { get; set; }
        public int IdEmployee { get; set; }

        public virtual Employees IdEmployeeNavigation { get; set; }
        public virtual RawStaff IdRawStaffNavigation { get; set; }
    }
}
