﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Products
    {
        public Products()
        {
            Ingredients = new HashSet<Ingredients>();
            ProductOnStorage = new HashSet<ProductOnStorage>();
            ProductOrderItems = new HashSet<ProductOrderItems>();
            RemovedProduct = new HashSet<RemovedProduct>();
        }

        public int IdProduct { get; set; }
        public string ProductName { get; set; }
        public decimal? Weight { get; set; }
        public string Desctiption { get; set; }
        public int? ExpirationTerm { get; set; }
        public decimal? Price { get; set; }
        public decimal? MarkUp { get; set; }
        public int? IdMeasure { get; set; }

        public virtual Measure IdMeasureNavigation { get; set; }
        public virtual ICollection<Ingredients> Ingredients { get; set; }
        public virtual ICollection<ProductOnStorage> ProductOnStorage { get; set; }
        public virtual ICollection<ProductOrderItems> ProductOrderItems { get; set; }
        public virtual ICollection<RemovedProduct> RemovedProduct { get; set; }
    }
}
