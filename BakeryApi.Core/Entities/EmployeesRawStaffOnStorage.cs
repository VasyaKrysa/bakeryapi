﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class EmployeesRawStaffOnStorage
    {
        public int IdEmployee { get; set; }
        public int IdRawStaffOnStorage { get; set; }

        public virtual Employees IdEmployeeNavigation { get; set; }
        public virtual RawStaffOnStorage IdRawStaffOnStorageNavigation { get; set; }
    }
}
