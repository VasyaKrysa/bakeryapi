﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class RawStaffOrderItems
    {
        public int IdRawStaffOrderItem { get; set; }
        public int IdRawStaff { get; set; }
        public decimal? Amount { get; set; }
        public int IdOrder { get; set; }

        public virtual RawStaffOrders IdOrderNavigation { get; set; }
        public virtual RawStaff IdRawStaffNavigation { get; set; }
    }
}
