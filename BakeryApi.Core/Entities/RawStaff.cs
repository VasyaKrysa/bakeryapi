﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class RawStaff
    {
        public RawStaff()
        {
            Ingredients = new HashSet<Ingredients>();
            RawStaffOnStorage = new HashSet<RawStaffOnStorage>();
            RawStaffOrderItems = new HashSet<RawStaffOrderItems>();
            RemovedRowStaff = new HashSet<RemovedRowStaff>();
            SuppliersRawStaff = new HashSet<SuppliersRawStaff>();
        }

        public int IdRawStaff { get; set; }
        public string Name { get; set; }
        public int? ExpirationDate { get; set; }
        public decimal? PricePerMeasure { get; set; }
        public int? IdMeasure { get; set; }

        public virtual Measure IdMeasureNavigation { get; set; }
        public virtual ICollection<Ingredients> Ingredients { get; set; }
        public virtual ICollection<RawStaffOnStorage> RawStaffOnStorage { get; set; }
        public virtual ICollection<RawStaffOrderItems> RawStaffOrderItems { get; set; }
        public virtual ICollection<RemovedRowStaff> RemovedRowStaff { get; set; }
        public virtual ICollection<SuppliersRawStaff> SuppliersRawStaff { get; set; }
    }
}
