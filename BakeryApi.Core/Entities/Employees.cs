﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Employees
    {
        public Employees()
        {
            EmployeesProductOrders = new HashSet<EmployeesProductOrders>();
            EmployeesRawStaffOnStorage = new HashSet<EmployeesRawStaffOnStorage>();
            EmployeesRawStaffOrders = new HashSet<EmployeesRawStaffOrders>();
            ProductOnStorageEmployees = new HashSet<ProductOnStorageEmployees>();
            RemovedProduct = new HashSet<RemovedProduct>();
            RemovedRowStaff = new HashSet<RemovedRowStaff>();
        }

        public int IdEmployee { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Experience { get; set; }
        public int? IdAddress { get; set; }
        public int IdPosition { get; set; }

        public virtual Address IdAddressNavigation { get; set; }
        public virtual Positions IdPositionNavigation { get; set; }
        public virtual ICollection<EmployeesProductOrders> EmployeesProductOrders { get; set; }
        public virtual ICollection<EmployeesRawStaffOnStorage> EmployeesRawStaffOnStorage { get; set; }
        public virtual ICollection<EmployeesRawStaffOrders> EmployeesRawStaffOrders { get; set; }
        public virtual ICollection<ProductOnStorageEmployees> ProductOnStorageEmployees { get; set; }
        public virtual ICollection<RemovedProduct> RemovedProduct { get; set; }
        public virtual ICollection<RemovedRowStaff> RemovedRowStaff { get; set; }
    }
}
