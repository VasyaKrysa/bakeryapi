﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class RemovedProduct
    {
        public int IdRemovedProduct { get; set; }
        public string ReasonToRemove { get; set; }
        public DateTime? RemovingDate { get; set; }
        public int IdEmployee { get; set; }
        public int? Amount { get; set; }
        public int IdProduct { get; set; }

        public virtual Employees IdEmployeeNavigation { get; set; }
        public virtual Products IdProductNavigation { get; set; }
    }
}
