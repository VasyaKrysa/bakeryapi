﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class ProductOnStorage
    {
        public ProductOnStorage()
        {
            ProductOnStorageEmployees = new HashSet<ProductOnStorageEmployees>();
        }

        public int IdProductOnStorage { get; set; }
        public int IdProduct { get; set; }
        public int IdStorage { get; set; }
        public int? Amount { get; set; }
        public DateTime? DateOfManufacture { get; set; }

        public virtual Products IdProductNavigation { get; set; }
        public virtual Storages IdStorageNavigation { get; set; }
        public virtual ICollection<ProductOnStorageEmployees> ProductOnStorageEmployees { get; set; }
    }
}
