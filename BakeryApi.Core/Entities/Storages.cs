﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Storages
    {
        public Storages()
        {
            ProductOnStorage = new HashSet<ProductOnStorage>();
            RawStaffOnStorage = new HashSet<RawStaffOnStorage>();
        }

        public int IdStorage { get; set; }
        public int? IdAddress { get; set; }

        public virtual Address IdAddressNavigation { get; set; }
        public virtual ICollection<ProductOnStorage> ProductOnStorage { get; set; }
        public virtual ICollection<RawStaffOnStorage> RawStaffOnStorage { get; set; }
    }
}
