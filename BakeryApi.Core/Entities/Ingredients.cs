﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Ingredients
    {
        public int IdIngredient { get; set; }
        public decimal? Weight { get; set; }
        public int? IdMeasure { get; set; }
        public int IdProduct { get; set; }
        public int IdRawStaff { get; set; }

        public virtual Measure IdMeasureNavigation { get; set; }
        public virtual Products IdProductNavigation { get; set; }
        public virtual RawStaff IdRawStaffNavigation { get; set; }
    }
}
