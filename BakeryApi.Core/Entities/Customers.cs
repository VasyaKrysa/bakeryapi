﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Customers
    {
        public Customers()
        {
            ProductOrders = new HashSet<ProductOrders>();
        }

        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int? IdAddress { get; set; }

        public virtual Address IdAddressNavigation { get; set; }
        public virtual ICollection<ProductOrders> ProductOrders { get; set; }
    }
}
