﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class ProductOrderItems
    {
        public int IdProductOrderItem { get; set; }
        public int IdProduct { get; set; }
        public int? Amount { get; set; }
        public int IdOrder { get; set; }

        public virtual ProductOrders IdOrderNavigation { get; set; }
        public virtual Products IdProductNavigation { get; set; }
    }
}
