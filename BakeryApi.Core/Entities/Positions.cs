﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Positions
    {
        public Positions()
        {
            Employees = new HashSet<Employees>();
        }

        public int IdPosition { get; set; }
        public string PositionName { get; set; }
        public int? Salary { get; set; }

        public virtual ICollection<Employees> Employees { get; set; }
    }
}
