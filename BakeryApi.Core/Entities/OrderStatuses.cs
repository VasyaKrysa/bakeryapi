﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class OrderStatuses
    {
        public OrderStatuses()
        {
            ProductOrders = new HashSet<ProductOrders>();
            RawStaffOrders = new HashSet<RawStaffOrders>();
        }

        public int IdOrderStatus { get; set; }
        public string Status { get; set; }

        public virtual ICollection<ProductOrders> ProductOrders { get; set; }
        public virtual ICollection<RawStaffOrders> RawStaffOrders { get; set; }
    }
}
