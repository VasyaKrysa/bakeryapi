﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Suppliers
    {
        public Suppliers()
        {
            RawStaffOrders = new HashSet<RawStaffOrders>();
            SuppliersRawStaff = new HashSet<SuppliersRawStaff>();
        }

        public int IdSupplier { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int? IdAddress { get; set; }

        public virtual Address IdAddressNavigation { get; set; }
        public virtual ICollection<RawStaffOrders> RawStaffOrders { get; set; }
        public virtual ICollection<SuppliersRawStaff> SuppliersRawStaff { get; set; }
    }
}
