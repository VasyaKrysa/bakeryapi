﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class Measure
    {
        public Measure()
        {
            Ingredients = new HashSet<Ingredients>();
            Products = new HashSet<Products>();
            RawStaff = new HashSet<RawStaff>();
        }

        public int IdMeasure { get; set; }
        public string Name { get; set; }
        public decimal? PartOfKg { get; set; }

        public virtual ICollection<Ingredients> Ingredients { get; set; }
        public virtual ICollection<Products> Products { get; set; }
        public virtual ICollection<RawStaff> RawStaff { get; set; }
    }
}
