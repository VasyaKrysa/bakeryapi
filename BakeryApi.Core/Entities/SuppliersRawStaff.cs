﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class SuppliersRawStaff
    {
        public int IdSupplier { get; set; }
        public int IdRawStaff { get; set; }

        public virtual RawStaff IdRawStaffNavigation { get; set; }
        public virtual Suppliers IdSupplierNavigation { get; set; }
    }
}
