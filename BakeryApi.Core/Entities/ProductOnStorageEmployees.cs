﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class ProductOnStorageEmployees
    {
        public int IdProductOnStorage { get; set; }
        public int IdEmployee { get; set; }

        public virtual Employees IdEmployeeNavigation { get; set; }
        public virtual ProductOnStorage IdProductOnStorageNavigation { get; set; }
    }
}
