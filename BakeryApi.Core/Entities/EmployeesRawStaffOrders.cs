﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class EmployeesRawStaffOrders
    {
        public int IdEmployee { get; set; }
        public int IdOrder { get; set; }

        public virtual Employees IdEmployeeNavigation { get; set; }
        public virtual RawStaffOrders IdOrderNavigation { get; set; }
    }
}
