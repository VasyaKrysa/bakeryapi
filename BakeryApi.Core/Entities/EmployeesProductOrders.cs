﻿using System;
using System.Collections.Generic;

namespace BakeryApi.Core.Entities
{
    public partial class EmployeesProductOrders
    {
        public int IdEmployee { get; set; }
        public int IdOrder { get; set; }

        public virtual Employees IdEmployeeNavigation { get; set; }
        public virtual ProductOrders IdOrderNavigation { get; set; }
    }
}
