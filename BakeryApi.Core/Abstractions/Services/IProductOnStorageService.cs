﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IProductOnStorageService
    {
        public List<ProductOnStorageDTO> GetAll();

        public ProductOnStorageDTO GetById(int id);

        public List<EmployeeDTO> GetEmployeesByProductId(int id);

        public ProductOnStorageDTO Insert(InsertingProductOnStorageDTO product);

        public ProductOnStorageDTO Update(ProductOnStorageDTO product);

        public void Delete(int id);
    }
}
