﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IPositionsService
    {
        public List<Positions> GetAll();

        public Positions GetByName(string name);

        public Positions Insert(Positions position);

        public Positions Update(Positions position);

        public void Delete(int id);
    }
}
