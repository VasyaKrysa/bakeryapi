﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IRawStaffOnStorageService
    {
        public List<RawStaffOnStorageDTO> GetAll();

        public RawStaffOnStorageDTO GetById(int id);

        public List<EmployeeDTO> GetEmployeesByRawStaffId(int id);

        public RawStaffOnStorageDTO Insert(InsertingRawStaffOnStorageDTO rawStaff);

        public RawStaffOnStorageDTO Update(RawStaffOnStorageDTO rawStaff);

        public void Delete(int id);
    }
}
