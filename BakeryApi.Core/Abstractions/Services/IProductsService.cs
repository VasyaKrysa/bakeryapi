﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IProductsService
    {
        public List<ProductDTO> GetAll();

        public ProductDTO GetById(int id);

        public ProductDTO Insert(ProductDTO product);

        public ProductDTO Update(ProductDTO product);

        public void Delete(int id);
    }
}
