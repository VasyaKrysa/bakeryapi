﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IRemovedProductsService
    {
        public List<RemovedProductDTO> GetAll();

        public RemovedProductDTO GetById(int id);

        public RemovedProductDTO RemoveFromStorage(RemovedProductDTO removedProduct, int productOnStorageId);

        public void Delete(int id);
    }
}
