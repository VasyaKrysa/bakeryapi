﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IProductsOrdersService
    {
        public List<ProductsOrderDTO> GetAll();

        public ProductsOrderDTO GetById(int id);

        public List<ProductsOrderDTO> GetByCustomerId(int customerId);

        public List<EmployeeDTO> GetEmployeesByOrderId(int id);

        public ProductsOrderDTO Insert(ProductOrders order);

        public bool ExecuteOrder(int orderId, List<int> employeesId);

        public bool PlaceAnOrder(int orderId);

        public ProductsOrderDTO Update(ProductOrders order);

        public void Delete(int id);
    }
}
