﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface ISuppliersService
    {
        public List<SupplierDTO> GetAll();

        public SupplierDTO GetById(int id);

        public SupplierDTO Insert(SupplierDTO supplier);

        public SupplierDTO Update(SupplierDTO supplier);

        public void AddRawStaffToSupplier(int supplierID, int rawStaffID);

        public List<RawStaffDTO> GetAllRawStaff(int supplierID);

        public void Delete(int id);
    }
}
