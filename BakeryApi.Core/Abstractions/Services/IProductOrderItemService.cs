﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IProductOrderItemService
    {
        public List<ProductOrderItemDTO> GetAll();

        public ProductOrderItemDTO GetById(int id);

        public List<ProductOrderItemDTO> GetByOrderId(int orderId);
        public List<ProductOrderItemDTO> InsertRange(List<ProductOrderItemDTO> orderItems);
        public ProductOrderItemDTO Insert(ProductOrderItemDTO orderItem);

        public ProductOrderItemDTO Update(ProductOrderItemDTO orderItem);

        public void Delete(int id);
    }
}
