﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IRawStaffOrderService
    {
        public List<RawStaffOrderDTO> GetAll();

        public RawStaffOrderDTO GetById(int id);

        public List<RawStaffOrderDTO> GetBySupplierId(int supplierId);

        public List<EmployeeDTO> GetEmployeesByOrderId(int id);

        public RawStaffOrderDTO Insert(RawStaffOrders order);

        public RawStaffOrderDTO Update(RawStaffOrders order);

        public void Delete(int id);
    }
}
