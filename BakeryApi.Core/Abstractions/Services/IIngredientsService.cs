﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IIngredientsService
    {
        public List<IngredientDTO> GetAll();

        public IngredientDTO GetById(int id);

        public List<IngredientDTO> GetByProductId(int productId);

        public IngredientDTO Insert(IngredientDTO ingredient);
        public List<IngredientDTO> InsertRange(List<IngredientDTO> ingredient);

        public IngredientDTO Update(IngredientDTO ingredient);

        public void Delete(int id);
    }
}
