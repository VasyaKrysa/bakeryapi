﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IRawStaffService
    {
        public List<RawStaffDTO> GetAll();

        public RawStaffDTO GetById(int id);

        public List<RawStaffDTO> GetByName(string rawStaffName);

        public RawStaffDTO Insert(RawStaffDTO product);

        public RawStaffDTO Update(RawStaffDTO product);

        public void Delete(int id);
    }
}
