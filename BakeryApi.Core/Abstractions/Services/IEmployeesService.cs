﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IEmployeesService
    {
        public List<EmployeeDTO> GetAll();

        public EmployeeDTO GetById(int id);

        public EmployeeDTO Insert(EmployeeDTO employee);

        public EmployeeDTO Update(EmployeeDTO employee);

        public void Delete(int id);
    }
}
