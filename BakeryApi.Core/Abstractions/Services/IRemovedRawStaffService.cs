﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IRemovedRawStaffService
    {
        public List<RemovedRawStaffDTO> GetAll();

        public RemovedRawStaffDTO GetById(int id);

        public RemovedRawStaffDTO RemoveFromStorage(RemovedRawStaffDTO removedRawStaff, int rawStaffOnStorageId);

        public void Delete(int id);
    }
}
