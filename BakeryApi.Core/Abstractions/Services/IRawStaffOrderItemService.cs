﻿using BakeryApi.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IRawStaffOrderItemService
    {
        public List<RawStaffOrderItemDTO> GetAll();

        public RawStaffOrderItemDTO GetById(int id);

        public List<RawStaffOrderItemDTO> GetByOrderId(int orderId);

        public RawStaffOrderItemDTO Insert(RawStaffOrderItemDTO orderItem);

        public List<RawStaffOrderItemDTO> InsertRange(List<RawStaffOrderItemDTO> orderItems);

        public RawStaffOrderItemDTO Update(RawStaffOrderItemDTO orderItem);

        public void Delete(int id);
    }
}
