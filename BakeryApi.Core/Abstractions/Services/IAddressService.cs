﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IAddressService
    {
        public List<Address> GetAll();

        public Address GetById(int id);

        public Address Insert(Address address);

        public Address Update(Address address);

        public void Delete(int id);
    }
}
