﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface IMeasureService
    {
        public List<MeasureDTO> GetAll();

        public MeasureDTO GetByName(string name);

        public MeasureDTO Insert(MeasureDTO measure);

        public MeasureDTO Update(MeasureDTO measure);

        public void Delete(int id);
    }
}
