﻿using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Services
{
    public interface ICustomersService
    {
        public List<CustomerDTO> GetAll();

        public CustomerDTO GetById(int id);

        public CustomerDTO Insert(CustomerDTO customer);

        public CustomerDTO Update(CustomerDTO customer);

        public void Delete(int id);
    }
}
