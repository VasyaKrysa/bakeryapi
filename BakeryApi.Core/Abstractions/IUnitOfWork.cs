﻿using BakeryApi.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions
{
    public interface IUnitOfWork : IDisposable
    {
        public IAddressRepository AddressRepository { get; }

        public ICustomersRepository CustomersRepository { get; }

        public IEmployeesProductOrdersRepository EmployeesProductOrdersRepository { get; }

        public IEmployeesRawStaffOrdersRepository EmployeesRawStaffOrdersRepository { get; }

        public IEmployeesRawStaffOnStorageRepository EmployeesRawStaffOnStorageRepository { get; }

        public IEmployeesRepository EmployeesRepository { get; }

        public IIngredientsRepository IngredientsRepository { get; }

        public IMeasureRepository MeasureRepository { get; }

        public IOrderStatusesRepository OrderStatusesRepository { get; }

        public IPositionRepository PositionRepository { get; }

        public IProductOnStorageEmployeesRepository ProductOnStorageEmployeesRepository { get; }

        public IProductOnStorageRepository ProductOnStorageRepository { get; }

        public IProductOrderItemRepository ProductOrderItemRepository { get; }

        public IProductOrdersRepository ProductOrdersRepository { get; }
        
        public IProductsRepository ProductsRepository { get; }

        public IRawStaffOnStorageRepository RawStaffOnStorageRepository { get; }

        public IRawStaffOrderItemRepository RawStaffOrderItemRepository { get; }

        public IRawStaffOrdersRepository RawStaffOrdersRepository { get; }

        public IRawStaffRepository RawStaffRepository { get; }

        public IRemovedProductsRepository RemovedProductsRepository { get; }

        public IRemovedRawStaffRepository RemovedRawStaffRepository { get; }

        public IStoragesRepository StoragesRepository { get; }

        public ISuppliersRawStaffRepository SuppliersRawStaffRepository { get; }

        public ISuppliersRepository SuppliersRepository { get; }
        
        void Save();
    }
}