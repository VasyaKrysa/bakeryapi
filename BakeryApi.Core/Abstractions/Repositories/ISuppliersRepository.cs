﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface ISuppliersRepository:IRepository<Suppliers>
    {
        public Suppliers GetById(int id);

        public void Delete(int id);

        public void AddRawStaffToSupplier(int supplierID, int rawStaffID);

        public IQueryable<RawStaff> GetAllRawStaff(int supplierID);
    }
}
