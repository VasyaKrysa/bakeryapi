﻿using BakeryApi.Core.Entities;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface ICustomersRepository : IRepository<Customers>
    {
        public Customers GetById(int id);

        public void Delete(int id);
    }
}
