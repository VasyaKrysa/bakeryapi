﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IEmployeesRawStaffOrdersRepository : IRepository<EmployeesRawStaffOrders>
    {
        public IQueryable<EmployeesRawStaffOrders> GetByEmployeerId(int id);
        public void Delete(int EmployeeId, int OrderID);
    }
}
