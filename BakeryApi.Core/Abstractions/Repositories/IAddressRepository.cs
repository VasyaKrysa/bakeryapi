﻿using BakeryApi.Core.Entities;


namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IAddressRepository : IRepository<Address>
    {
        public Address GetById(int? id);

        public void Delete(int id);
    }
}
