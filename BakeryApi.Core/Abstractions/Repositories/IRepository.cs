﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IRepository<TEntity>
    {
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);

        TEntity Edit(TEntity entity);
    }
}
