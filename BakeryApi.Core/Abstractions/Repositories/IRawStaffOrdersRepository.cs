﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IRawStaffOrdersRepository:IRepository<RawStaffOrders>
    {
        public RawStaffOrders GetById(int id);

        public void Delete(int id);
    }
}
