﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IRemovedRawStaffRepository:IRepository<RemovedRowStaff>
    {
        public RemovedRowStaff GetById(int id);

        public void Delete(int id);
    }
}
