﻿using BakeryApi.Core.Entities;
using System.Linq;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IRawStaffRepository:IRepository<RawStaff>
    {
        public RawStaff GetById(int id);

        public void Delete(int id);

        public IQueryable<RawStaff> GetByName(string begin);
    }
}
