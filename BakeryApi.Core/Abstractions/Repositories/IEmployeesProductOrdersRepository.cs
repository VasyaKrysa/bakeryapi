﻿using BakeryApi.Core.Entities;
using System.Linq;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface IEmployeesProductOrdersRepository : IRepository<EmployeesProductOrders>
    {
        public IQueryable<EmployeesProductOrders> GetByEmployeerId(int id);
        public void Delete(int EmployeeId, int OrderID);
    }
}
