﻿using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Core.Abstractions.Repositories
{
    public interface ISuppliersRawStaffRepository:IRepository<SuppliersRawStaff>
    {
        public IQueryable<SuppliersRawStaff> GetBySupplierId(int id);

        public void Delete(int Suplierid, int RawStaffId);

        public void DeleteAll(int Suplierid);

    }
}
