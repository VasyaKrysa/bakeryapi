﻿using AutoMapper;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BakeryApi.Core
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Ingredients, IngredientDTO>().ReverseMap();
            CreateMap<Products, ProductDTO>().ReverseMap();
            CreateMap<ProductOnStorage, ProductOnStorageDTO>().ReverseMap();
            CreateMap<ProductOrderItems, ProductOrderItemDTO>().ReverseMap();
            CreateMap<Address, AddressDTO>().ReverseMap();
            CreateMap<PositionDTO, Positions>().ReverseMap();
            CreateMap<Measure, MeasureDTO>().ReverseMap();
            CreateMap<OrderStatuses, OrderStatusDTO>().ReverseMap();

            CreateMap<RawStaff, RawStaffDTO>().ForMember(rawStaffDTO => rawStaffDTO.MeasureName, opt => opt.MapFrom(rawStaff => rawStaff.IdMeasureNavigation.Name));
            CreateMap<RawStaffDTO, RawStaff>();

            CreateMap<RawStaffOnStorage, RawStaffOnStorageDTO>().ForMember(itemDTO => itemDTO.RawStaff, opt => opt.MapFrom(item => item.IdRawStaffNavigation))
                .ForMember(itemDTO => itemDTO.StorageAddress, opt => opt.MapFrom(item => item.IdStorageNavigation.IdAddressNavigation));
            CreateMap<RawStaffOrderItemDTO, RawStaffOrderItems>();

            CreateMap<RawStaffOrderItems, RawStaffOrderItemDTO>().ForMember(itemDTO => itemDTO.RawStaff, opt => opt.MapFrom(item => item.IdRawStaffNavigation))
                .ForMember(itemDTO => itemDTO.Order, opt => opt.MapFrom(item => item.IdOrderNavigation));
            CreateMap<RawStaffOrderItemDTO, RawStaffOrderItems>();

            CreateMap<Storages, StorageDTO>().ForMember(storageDTO => storageDTO.Address, opt => opt.MapFrom(storage => storage.IdAddressNavigation));
            CreateMap<StorageDTO, Storages>().ForMember(storage => storage.IdAddressNavigation, opt => opt.MapFrom(storageDTO => storageDTO.Address));

            CreateMap<Employees, EmployeeDTO>().ForMember(empDTO => empDTO.Address, opt => opt.MapFrom(emp => emp.IdAddressNavigation))
                .ForMember(empDTO => empDTO.Position, opt => opt.MapFrom(emp => emp.IdPositionNavigation));
            CreateMap<EmployeeDTO, Employees>();

            CreateMap<RemovedProduct, RemovedProductDTO>().ForMember(prodDTO => prodDTO.Product, opt => opt.MapFrom(prod => prod.IdProductNavigation))
                .ForMember(prodDTO => prodDTO.Employee, opt => opt.MapFrom(prod => prod.IdEmployeeNavigation));
            CreateMap<RemovedProductDTO, RemovedProduct>();

            CreateMap<RemovedRowStaff, RemovedRawStaffDTO>().ForMember(rawStaffDTO => rawStaffDTO.RawStaff, opt => opt.MapFrom(rawStaff => rawStaff.IdRawStaffNavigation))
                .ForMember(rawStaffDTO => rawStaffDTO.Employee, opt => opt.MapFrom(rawStaff => rawStaff.IdEmployeeNavigation));
            CreateMap<RemovedRawStaffDTO, RemovedRowStaff>();

            CreateMap<Customers, CustomerDTO>().ForMember(prodDTO => prodDTO.Address, opt => opt.MapFrom(cuss => cuss.IdAddressNavigation));
            CreateMap<CustomerDTO, Customers>().ForMember(prod => prod.IdAddressNavigation, opt => opt.MapFrom(cuss => cuss.Address)); 

            CreateMap<Suppliers, SupplierDTO>().ForMember(supplierDTO => supplierDTO.Address, opt => opt.MapFrom(supplier => supplier.IdAddressNavigation));
            CreateMap<SupplierDTO, Suppliers>().ForMember(supplier => supplier.IdAddressNavigation, opt => opt.MapFrom(supplierDTO => supplierDTO.Address));


            CreateMap<ProductOrders, ProductsOrderDTO>().ForMember(prodDTO=>prodDTO.Customer, opt=>opt.MapFrom(prod=>prod.IdCustomerNavigation))
                .ForMember(prodDTO => prodDTO.Status, opt => opt.MapFrom(prod => prod.IdOrderStatusNavigation.Status));
            CreateMap<ProductsOrderDTO, ProductOrders>();

            CreateMap<RawStaffOrders, RawStaffOrderDTO>().ForMember(orderDTO => orderDTO.Supplier, opt => opt.MapFrom(order => order.IdSupplierNavigation))
               .ForMember(orderDTO => orderDTO.Status, opt => opt.MapFrom(order => order.IdOrderStatusNavigation.Status));
            CreateMap<RawStaffOrderDTO, RawStaffOrders>();
        }
    }
}
