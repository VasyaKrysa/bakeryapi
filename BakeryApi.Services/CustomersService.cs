﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class CustomersService:ICustomersService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public CustomersService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.CustomersRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<CustomerDTO> GetAll()
        {
            var customers = _unitOfWork.CustomersRepository.GetAll().ToList();
            return _mapper.Map<List<Customers>, List<CustomerDTO>>(customers);
        }

        public CustomerDTO GetById(int id)
        {
            var customer = _unitOfWork.CustomersRepository.GetById(id);
            return _mapper.Map<Customers,CustomerDTO>(customer);
        }

        public CustomerDTO Insert(CustomerDTO customer)
        {
            var cus = _mapper.Map<CustomerDTO, Customers>(customer);
            _unitOfWork.CustomersRepository.Add(cus);
            _unitOfWork.Save();
            customer = _mapper.Map<Customers, CustomerDTO>(cus);

            return customer;
        }

        public CustomerDTO Update(CustomerDTO customer)
        {
            var cus = _mapper.Map<CustomerDTO, Customers>(customer);
            _unitOfWork.AddressRepository.Edit(cus.IdAddressNavigation);
            _unitOfWork.CustomersRepository.Edit(cus);
            _unitOfWork.Save();

            return customer;
        }
    }
}
