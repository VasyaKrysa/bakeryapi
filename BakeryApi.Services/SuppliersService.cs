﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using Microsoft.EntityFrameworkCore;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class SuppliersService: ISuppliersService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public SuppliersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            int addressID = (int)_unitOfWork.SuppliersRepository.GetById(id).IdAddress;
            _unitOfWork.SuppliersRepository.Delete(id);
            _unitOfWork.AddressRepository.Delete(addressID);
            _unitOfWork.Save();
        }

        public List<SupplierDTO> GetAll()
        {
            var suppliers = _unitOfWork.SuppliersRepository.GetAll()
                .Include(s => s.IdAddressNavigation).ToList();
            return _mapper.Map<List<Suppliers>, List<SupplierDTO>>(suppliers);
        }

        public SupplierDTO GetById(int id)
        {
            var supplier = _unitOfWork.SuppliersRepository.GetById(id);
            return _mapper.Map<Suppliers, SupplierDTO>(supplier);
        }

        public SupplierDTO Insert(SupplierDTO supplier)
        {
            var sup = _mapper.Map<SupplierDTO, Suppliers>(supplier);
            _unitOfWork.SuppliersRepository.Add(sup);
            _unitOfWork.Save();
            supplier = _mapper.Map<Suppliers, SupplierDTO>(sup);

            return supplier;
        }

        public SupplierDTO Update(SupplierDTO supplier)
        {
            var sup = _mapper.Map<SupplierDTO, Suppliers>(supplier);
            _unitOfWork.SuppliersRepository.Edit(sup);
            _unitOfWork.Save();

            return supplier;
        }

        public void AddRawStaffToSupplier(int supplierID, int rawStaffID)
        {
            _unitOfWork.SuppliersRepository.AddRawStaffToSupplier(supplierID, rawStaffID);
        }

        public List<RawStaffDTO> GetAllRawStaff(int supplierID)
        {
            var rawStaff = _unitOfWork.SuppliersRepository.GetAllRawStaff(supplierID).ToList();
            var rawStaffDTOs = _mapper.Map<List<RawStaff>, List<RawStaffDTO>>(rawStaff);   
      
            return rawStaffDTOs;
        }
    }
}

