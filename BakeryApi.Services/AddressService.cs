﻿using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BakeryApi.Services
{
    public class AddressService : IAddressService
    {
        private IUnitOfWork _unitOfWork;
        public AddressService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _unitOfWork.AddressRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<Address> GetAll()
        {
            return _unitOfWork.AddressRepository.GetAll().ToList();
        }

        public Address GetById(int id)
        {
            return _unitOfWork.AddressRepository.GetById(id);
        }

        public Address Insert(Address address)
        {
            _unitOfWork.AddressRepository.Add(address);
            _unitOfWork.Save();

            return address;
        }

        public Address Update(Address address)
        {
            _unitOfWork.AddressRepository.Edit(address);
            _unitOfWork.Save();

            return address;
        }
    }
}
