﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class ProductOrdersService : IProductsOrdersService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private IProductOnStorageService _productOnStorageService;

        public ProductOrdersService(IUnitOfWork unitOfWork, IMapper mapper, IProductOnStorageService productOnStorageService)
        {
            _productOnStorageService = productOnStorageService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.ProductOrdersRepository.Delete(id);
            _unitOfWork.Save();
        }

        public bool ExecuteOrder(int orderId, List<int> employeesId)
        {
            var NeededProducts = new List<ProductOnStorageDTO>();
            var OrderItems = _unitOfWork.ProductOrderItemRepository.GetAll().Where(item => item.IdOrder == orderId).ToList();
            OrderItems.ForEach(item =>
            {
                var productAmount = _unitOfWork.ProductOnStorageRepository.GetAll()
                .Where(prod => prod.IdProduct == item.IdProduct).Sum(prod => prod.Amount);
                if (productAmount < item.Amount)
                    NeededProducts.Add(new ProductOnStorageDTO()
                    {
                        Amount = item.Amount - productAmount,
                        IdProduct = item.IdProduct,
                        IdStorage = _unitOfWork.ProductOnStorageRepository.GetAll().FirstOrDefault().IdStorage
                    });
            });
            NeededProducts.ForEach(prod => {
                var insertedProd = new InsertingProductOnStorageDTO() { employeesId = employeesId, product = prod };
                _productOnStorageService.Insert(insertedProd);
                });

            employeesId.ForEach(emp => _unitOfWork.EmployeesProductOrdersRepository
            .Add(new EmployeesProductOrders()
            {
                IdEmployee = emp,
                IdOrder = orderId
            }));

            _unitOfWork.Save();

            return true;
        }

        public List<ProductsOrderDTO> GetAll()
        {
            var productOrders = _unitOfWork.ProductOrdersRepository.GetAll().ToList();
            var productsOrdersDTO = _mapper.Map<List<ProductOrders>, List<ProductsOrderDTO>>(productOrders);

            return productsOrdersDTO;
        }

        public List<ProductsOrderDTO> GetByCustomerId(int customerId)
        {
            var productOrders = _unitOfWork.ProductOrdersRepository.GetAll().
                Where(order => order.IdCustomer == customerId).ToList();
            var productsOrdersDTO = _mapper.Map<List<ProductOrders>, List<ProductsOrderDTO>>(productOrders);

            return productsOrdersDTO;
        }

        public ProductsOrderDTO GetById(int id)
        {
            var productOrder = _unitOfWork.ProductOrdersRepository.GetById(id);
            var productsOrderDTO = _mapper.Map<ProductOrders, ProductsOrderDTO>(productOrder);

            return productsOrderDTO;

        }

        public List<EmployeeDTO> GetEmployeesByOrderId(int id)
        {
            var EmployeesId = _unitOfWork.EmployeesProductOrdersRepository.GetAll().Where(prod => prod.IdOrder == id).ToList();
            var Employees = new List<Employees>();
            EmployeesId.ForEach(Emp =>
            {
                var Employee = _unitOfWork.EmployeesRepository.GetById(Emp.IdEmployee);
                Employees.Add(Employee);
            });

            return _mapper.Map<List<Employees>, List<EmployeeDTO>>(Employees); 
        }

        public ProductsOrderDTO Insert(ProductOrders order)
        {
            var customer = _unitOfWork.CustomersRepository.GetById(order.IdCustomer);
            order.Time = DateTime.UtcNow;
            if (customer == null)
            {
                if (order.IdCustomerNavigation.IdAddressNavigation == null)
                    throw new Exception("Could not create customer, no address");
            }
            _unitOfWork.ProductOrdersRepository.Add(order);
            _unitOfWork.Save();
            var orderDTO = _mapper.Map<ProductOrders, ProductsOrderDTO>(order);

            return orderDTO;
        }

        public bool PlaceAnOrder(int orderId)
        {
            var orderItem = _unitOfWork.ProductOrderItemRepository.GetAll().Where(item => item.IdOrder == orderId).ToList();
            orderItem.ForEach(item =>
            {
                var productsOnStorage = _unitOfWork.ProductOnStorageRepository.GetAll().Where(product => product.IdProduct == item.IdProduct).ToList();
                var Amount = item.Amount;
                var AllAmountProduct = productsOnStorage.Sum(product => product.Amount);
                if (AllAmountProduct < Amount)
                    throw new Exception("Not enough products on storage");
                int i = 0;
                while (Amount > 0)
                {
                    if (productsOnStorage[i].Amount > Amount)
                    {
                        productsOnStorage[i].Amount -= Amount;
                        _unitOfWork.ProductOnStorageRepository.Edit(productsOnStorage[i]);
                        Amount = 0;
                    }
                    else if (productsOnStorage[i].Amount == Amount)
                    {
                        _unitOfWork.ProductOnStorageRepository.Delete(productsOnStorage[i].IdProductOnStorage);
                        Amount = 0;
                    }
                    else if (productsOnStorage[i].Amount < Amount)
                    {
                        _unitOfWork.ProductOnStorageRepository.Delete(productsOnStorage[i].IdProductOnStorage);
                        Amount -= productsOnStorage[i].Amount;
                    }
                    i++;
                }
            });
            return true;
        }

        public ProductsOrderDTO Update(ProductOrders order)
        {
            _unitOfWork.ProductOrdersRepository.Edit(order);
            _unitOfWork.Save();
            var orderDTO = _mapper.Map<ProductOrders, ProductsOrderDTO>(order);

            return orderDTO;
        }
    }
}
