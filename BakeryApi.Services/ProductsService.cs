﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class ProductsService : IProductsService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public ProductsService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Delete(int id)
        {
            var ingredients = _unitOfWork.IngredientsRepository.GetAll().Where(ing => ing.IdProduct == id).ToList();
            ingredients.ForEach(ing => _unitOfWork.IngredientsRepository.Delete(ing.IdIngredient));
            _unitOfWork.ProductsRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<ProductDTO> GetAll()
        {
            var products = _unitOfWork.ProductsRepository.GetAll().ToList();
            var productsDto = _mapper.Map<List<Products>, List<ProductDTO>>(products);
            productsDto.ForEach(product => product.MeasureName = _unitOfWork.MeasureRepository.GetById(product.IdMeasure).Name);

            return productsDto;
        }

        public ProductDTO GetById(int id)
        {
            var product = _unitOfWork.ProductsRepository.GetById(id);
            var productDto = _mapper.Map<Products, ProductDTO>(product);
            productDto.MeasureName = _unitOfWork.MeasureRepository.GetById(productDto.IdMeasure).Name;

            return productDto;
        }

        public ProductDTO Insert(ProductDTO product)
        {
            var Product = _mapper.Map<ProductDTO, Products>(product);
            _unitOfWork.ProductsRepository.Add(Product);
            _unitOfWork.Save();
            product.IdProduct = Product.IdProduct;

            return product;
        }

        public ProductDTO Update(ProductDTO product)
        {
            var Product = _mapper.Map<ProductDTO, Products>(product);
            _unitOfWork.ProductsRepository.Edit(Product);
            _unitOfWork.Save();

            return product;
        }
    }
}
