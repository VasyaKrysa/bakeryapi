﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class RawStaffOrderItemService: IRawStaffOrderItemService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public RawStaffOrderItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.RawStaffOrderItemRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<RawStaffOrderItemDTO> GetAll()
        {
            var orderItems = _unitOfWork.RawStaffOrderItemRepository.GetAll()
                .Include(item => item.IdOrderNavigation)
                .Include(item => item.IdRawStaffNavigation)
                .ToList();
            var orderItemsDTO = _mapper.Map<List<RawStaffOrderItems>, List<RawStaffOrderItemDTO>>(orderItems);
            orderItemsDTO.ForEach(item =>
            {
                var order = _unitOfWork.RawStaffOrdersRepository.GetById(item.IdOrder);
                var orderDTO = _mapper.Map<RawStaffOrders, RawStaffOrderDTO>(order);
                item.Order = orderDTO;

                var rawStaff = _unitOfWork.RawStaffRepository.GetById(item.IdRawStaff);
                var rawStaffDTO = _mapper.Map<RawStaff, RawStaffDTO>(rawStaff);
                rawStaffDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(rawStaffDTO.IdMeasure).Name;
                item.RawStaff = rawStaffDTO;
            });

            return orderItemsDTO;
        }

        public RawStaffOrderItemDTO GetById(int id)
        {
            var orderItem = _unitOfWork.RawStaffOrderItemRepository.GetById(id);
            var orderItemDTO = _mapper.Map<RawStaffOrderItems, RawStaffOrderItemDTO>(orderItem);

            var order = _unitOfWork.RawStaffOrdersRepository.GetById(orderItemDTO.IdOrder);
            var orderDTO = _mapper.Map<RawStaffOrders, RawStaffOrderDTO>(order);
            orderItemDTO.Order = orderDTO;

            var rawStaff = _unitOfWork.RawStaffRepository.GetById(orderItemDTO.IdRawStaff);
            var rawStaffDTO = _mapper.Map<RawStaff, RawStaffDTO>(rawStaff);
            rawStaffDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(rawStaffDTO.IdMeasure).Name;
            orderItemDTO.RawStaff = rawStaffDTO;

            return orderItemDTO;
        }

        public List<RawStaffOrderItemDTO> GetByOrderId(int orderId)
        {
            var orderItems = _unitOfWork.RawStaffOrderItemRepository.GetAll().Where(rawStaff => rawStaff.IdOrder == orderId).ToList();
            var orderItemsDTO = _mapper.Map<List<RawStaffOrderItems>, List<RawStaffOrderItemDTO>>(orderItems);
            orderItemsDTO.ForEach(item =>
            {
                var order = _unitOfWork.RawStaffOrdersRepository.GetById(item.IdOrder);
                var orderDTO = _mapper.Map<RawStaffOrders, RawStaffOrderDTO>(order);
                item.Order = orderDTO;

                var rawStaff = _unitOfWork.RawStaffRepository.GetById(item.IdRawStaff);
                var rawStaffDTO = _mapper.Map<RawStaff, RawStaffDTO>(rawStaff);
                rawStaffDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(rawStaffDTO.IdMeasure).Name;
                item.RawStaff = rawStaffDTO;
            });

            return orderItemsDTO;
        }

        public RawStaffOrderItemDTO Insert(RawStaffOrderItemDTO orderItem)
        {
            var OrderItem = _mapper.Map<RawStaffOrderItemDTO, RawStaffOrderItems>(orderItem);
            _unitOfWork.RawStaffOrderItemRepository.Add(OrderItem);
            _unitOfWork.Save();
            orderItem.IdRawStaffOrderItem = OrderItem.IdRawStaffOrderItem;

            return orderItem;
        }

        public List<RawStaffOrderItemDTO> InsertRange(List<RawStaffOrderItemDTO> orderItems)
        {
            var OrderItems = _mapper.Map<List<RawStaffOrderItemDTO>, List<RawStaffOrderItems>>(orderItems);
            OrderItems.ForEach(item =>
            {
                _unitOfWork.RawStaffOrderItemRepository.Add(item);
            });
            _unitOfWork.Save();
            for (int i = 0; i < orderItems.Count; i++)
            {
                orderItems[i].IdRawStaffOrderItem = OrderItems[i].IdRawStaffOrderItem;
            }

            return orderItems;
        }

        public RawStaffOrderItemDTO Update(RawStaffOrderItemDTO orderItem)
        {
            var OrderItem = _mapper.Map<RawStaffOrderItemDTO, RawStaffOrderItems>(orderItem);
            _unitOfWork.RawStaffOrderItemRepository.Edit(OrderItem);
            _unitOfWork.Save();

            return orderItem;
        }
    }
}
