﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class ProductOnStorageService:IProductOnStorageService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public ProductOnStorageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.ProductOnStorageRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<ProductOnStorageDTO> GetAll()
        {
            var products = _unitOfWork.ProductOnStorageRepository.GetAll().ToList();
            var productsDTO = _mapper.Map<List<ProductOnStorage>, List<ProductOnStorageDTO>>(products);
            productsDTO.ForEach(prod =>
            {
                var idAddress = _unitOfWork.StoragesRepository.GetById(prod.IdStorage).IdAddress;
                prod.StorageAddress = _mapper.Map<Address,AddressDTO>(_unitOfWork.AddressRepository.GetById(idAddress));
                var product = _unitOfWork.ProductsRepository.GetById(prod.IdProduct);
                var productDTO = _mapper.Map<Products, ProductDTO>(product);
                productDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(productDTO.IdMeasure).Name;
                prod.Product = productDTO;
            });

            return productsDTO;
        }

        public ProductOnStorageDTO GetById(int id)
        {
            var product = _unitOfWork.ProductOnStorageRepository.GetById(id);
            var productDTO = _mapper.Map<ProductOnStorage, ProductOnStorageDTO>(product);
       
            var idAddress = _unitOfWork.StoragesRepository.GetById(productDTO.IdStorage).IdAddress;
            productDTO.StorageAddress = _mapper.Map<Address, AddressDTO>(_unitOfWork.AddressRepository.GetById(idAddress));
            var tempProduct = _unitOfWork.ProductsRepository.GetById(productDTO.IdProduct);
            var tempProductDTO = _mapper.Map<Products, ProductDTO>(tempProduct);
            tempProductDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(tempProductDTO.IdMeasure).Name;
            productDTO.Product = tempProductDTO;

            return productDTO;
        }

        public List<EmployeeDTO> GetEmployeesByProductId(int id)
        {
            var EmployeesId = _unitOfWork.ProductOnStorageEmployeesRepository.GetAll().Where(prod => prod.IdProductOnStorage == id).ToList();
            var Employees = new List<Employees>();
            EmployeesId.ForEach(Emp =>
            {
                var Employee = _unitOfWork.EmployeesRepository.GetById(Emp.IdEmployee);
                Employees.Add(Employee);
            });

            return _mapper.Map<List<Employees>,List<EmployeeDTO>>(Employees);
        }

        public ProductOnStorageDTO Insert(InsertingProductOnStorageDTO productDTO)
        {
            var product = productDTO.product;
            var employeesId = productDTO.employeesId;
            var ingredients = _unitOfWork.IngredientsRepository.GetAll().Where(ing => ing.IdProduct == product.IdProduct).ToList();
            ingredients.ForEach(ing =>
            {
                var rawStuff = _unitOfWork.RawStaffRepository.GetById(ing.IdRawStaff);
                var rawStuffs = _unitOfWork.RawStaffOnStorageRepository.GetAll().Where(raw => raw.IdRawStaff == ing.IdRawStaff).ToList();
                var rawStuffPartOfKg = _unitOfWork.MeasureRepository.GetById(rawStuff.IdMeasure).PartOfKg;
                var rawStufsWeight = rawStuffs.Sum(raw => raw.Amount) * rawStuffPartOfKg;
                var ingWeight = ing.Weight * _unitOfWork.MeasureRepository.GetById(ing.IdMeasure).PartOfKg*product.Amount;
                if (ingWeight > rawStufsWeight)
                {
                    throw new Exception("Not enough raw stuff " + rawStuff.Name + " to make the product");
                }
                else
                {
                    int i = 0;
                    while(ingWeight>0)
                    {
                        if(rawStuffs[i].Amount*rawStuffPartOfKg>ingWeight)
                        {
                            rawStuffs[i].Amount -= (ingWeight / rawStuffPartOfKg);
                            _unitOfWork.RawStaffOnStorageRepository.Edit(rawStuffs[i]);
                            ingWeight = 0;
                        }
                        else if(rawStuffs[i].Amount * rawStuffPartOfKg == ingWeight)
                        {
                            _unitOfWork.RawStaffOnStorageRepository.Delete(rawStuffs[i].IdRawStaffOnStorage);
                            ingWeight = 0;
                        }
                        else if(rawStuffs[i].Amount * rawStuffPartOfKg < ingWeight)
                        {
                            _unitOfWork.RawStaffOnStorageRepository.Delete(rawStuffs[i].IdRawStaffOnStorage);
                            ingWeight -= rawStuffs[i].Amount * rawStuffPartOfKg;
                        }
                        i++;
                    }
                }
            });
            var productOnStorage = _mapper.Map<ProductOnStorageDTO, ProductOnStorage>(product);
            productOnStorage.DateOfManufacture = DateTime.UtcNow;
            _unitOfWork.ProductOnStorageRepository.Add(productOnStorage);
            product.DateOfManufacture = productOnStorage.DateOfManufacture;
            _unitOfWork.Save();

            employeesId.ForEach(id =>
            {
                _unitOfWork.ProductOnStorageEmployeesRepository.Add(new ProductOnStorageEmployees()
                {
                    IdEmployee = id,
                    IdProductOnStorage = productOnStorage.IdProductOnStorage
                });
            });
            _unitOfWork.Save();

            product.IdProductOnStorage = productOnStorage.IdProductOnStorage;

            return product;
        }

        public ProductOnStorageDTO Update(ProductOnStorageDTO product)
        {
            throw new NotImplementedException();
        }
    }
}
