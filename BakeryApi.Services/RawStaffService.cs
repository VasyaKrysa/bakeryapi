﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class RawStaffService: IRawStaffService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public RawStaffService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Delete(int id)
        {
            _unitOfWork.RawStaffRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<RawStaffDTO> GetAll()
        {
            var rawStaff = _unitOfWork.RawStaffRepository.GetAll()
                .Include(rs => rs.IdMeasureNavigation).ToList();
            var rawStaffDTO = _mapper.Map<List<RawStaff>, List<RawStaffDTO>>(rawStaff);

            return rawStaffDTO;
        }

        public RawStaffDTO GetById(int id)
        {
            var rawStaff = _unitOfWork.RawStaffRepository.GetById(id);
            var rawStaffDTO = _mapper.Map<RawStaff, RawStaffDTO>(rawStaff);

            return rawStaffDTO;
        }

        public List<RawStaffDTO> GetByName(string rawStaffName)
        {
            var rawStaff = _unitOfWork.RawStaffRepository.GetByName(rawStaffName)
                .Include(rs => rs.IdMeasureNavigation).ToList();
            var rawStaffDTOs = _mapper.Map<List<RawStaff>, List<RawStaffDTO>>(rawStaff);
            return rawStaffDTOs;
        }

        public RawStaffDTO Insert(RawStaffDTO rawStaffDTO)
        {
            var rawStaff = _mapper.Map<RawStaffDTO, RawStaff>(rawStaffDTO);
            _unitOfWork.RawStaffRepository.Add(rawStaff);
            _unitOfWork.Save();
            rawStaffDTO.IdRawStaff = rawStaff.IdRawStaff;
            rawStaffDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(rawStaffDTO.IdMeasure).Name;

            return rawStaffDTO;
        }

        public RawStaffDTO Update(RawStaffDTO rawStaffDTO)
        {
            var rawStaff = _mapper.Map<RawStaffDTO, RawStaff>(rawStaffDTO);
            _unitOfWork.RawStaffRepository.Edit(rawStaff);
            _unitOfWork.Save();

            return rawStaffDTO;
        }
    }
}
