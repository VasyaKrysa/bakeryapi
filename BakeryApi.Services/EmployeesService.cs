﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class EmployeesService:IEmployeesService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public EmployeesService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _unitOfWork.EmployeesRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<EmployeeDTO> GetAll()
        {
            var employees = _unitOfWork.EmployeesRepository.GetAll()
                .Include(empl => empl.IdAddressNavigation)
                .Include(empl => empl.IdPositionNavigation)
                .ToList();

            return _mapper.Map<List<Employees>,List<EmployeeDTO>>(employees);
        }

        public EmployeeDTO GetById(int id)
        {
            return _mapper.Map<Employees,EmployeeDTO>(_unitOfWork.EmployeesRepository.GetById(id));
        }

        public EmployeeDTO Insert(EmployeeDTO employee)
        {
            var emp = _mapper.Map<EmployeeDTO, Employees>(employee);
            _unitOfWork.EmployeesRepository.Add(emp);
            _unitOfWork.Save();
            employee.IdEmployee = emp.IdEmployee;

            return employee;
        }

        public EmployeeDTO Update(EmployeeDTO employee)
        {
            var emp = _mapper.Map<EmployeeDTO, Employees>(employee);
            _unitOfWork.EmployeesRepository.Edit(emp);
            _unitOfWork.Save();

            return employee;
        }
    }
}
