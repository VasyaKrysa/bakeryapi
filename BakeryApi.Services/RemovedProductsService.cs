﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class RemovedProductsService:IRemovedProductsService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper; 

        public RemovedProductsService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _unitOfWork.RemovedProductsRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<RemovedProductDTO> GetAll()
        {
            var products = _unitOfWork.RemovedProductsRepository.GetAll().ToList();
            return _mapper.Map<List<RemovedProduct>,List<RemovedProductDTO>>( products);
        }

        public RemovedProductDTO GetById(int id)
        {
            return _mapper.Map<RemovedProduct,RemovedProductDTO>(_unitOfWork.RemovedProductsRepository.GetById(id));
        }

        public RemovedProductDTO RemoveFromStorage(RemovedProductDTO removedProduct, int productOnStorageId)
        {
            var productOnStorage = _unitOfWork.ProductOnStorageRepository.GetById(productOnStorageId);
            removedProduct.IdProduct = productOnStorage.IdProduct;
            removedProduct.RemovingDate = DateTime.UtcNow;
            if (productOnStorage.Amount < removedProduct.Amount)
                throw new Exception("Not enough products on storage");

            if (productOnStorage.Amount == removedProduct.Amount)
                _unitOfWork.ProductOnStorageRepository.Delete(productOnStorageId);

            if (productOnStorage.Amount > removedProduct.Amount)
            {
                productOnStorage.Amount -= removedProduct.Amount;
                _unitOfWork.ProductOnStorageRepository.Edit(productOnStorage);
            }

            var remProd = _mapper.Map<RemovedProductDTO, RemovedProduct>(removedProduct);
            _unitOfWork.RemovedProductsRepository.Add(remProd);
            _unitOfWork.Save();

            removedProduct.IdRemovedProduct = remProd.IdRemovedProduct;

            return removedProduct;
        }
    }
}
