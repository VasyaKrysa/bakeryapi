﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BakeryApi.Services
{
    public class MeasureService : IMeasureService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public MeasureService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.MeasureRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<MeasureDTO> GetAll()
        {
            var measures = _unitOfWork.MeasureRepository.GetAll().ToList();
            return _mapper.Map<List<Measure>, List<MeasureDTO>>(measures);
        }

        public MeasureDTO GetByName(string name)
        {
            var measure = _unitOfWork.MeasureRepository.GetAll().Where(measure => measure.Name == name).FirstOrDefault();
            return _mapper.Map<Measure,MeasureDTO>(measure);
        }

        public MeasureDTO Insert(MeasureDTO measure)
        {
            var mes=_mapper.Map<MeasureDTO, Measure>(measure);
            _unitOfWork.MeasureRepository.Add(mes);
            _unitOfWork.Save();
            measure = _mapper.Map<Measure, MeasureDTO>(mes);

            return measure;
        }

        public MeasureDTO Update(MeasureDTO measure)
        {
            var mes = _mapper.Map<MeasureDTO, Measure>(measure);
            _unitOfWork.MeasureRepository.Add(mes);
            _unitOfWork.Save();
            measure = _mapper.Map<Measure, MeasureDTO>(mes);

            return measure;
        }
    }
}
