﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class RawStaffOrderService: IRawStaffOrderService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private IRawStaffOnStorageService _rawStaffOnStorageService;

        public RawStaffOrderService(IUnitOfWork unitOfWork, IMapper mapper, IRawStaffOnStorageService rawStaffOnStorageService)
        {
            _rawStaffOnStorageService = rawStaffOnStorageService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.RawStaffOrdersRepository.Delete(id);
            _unitOfWork.Save();
        }


        public List<RawStaffOrderDTO> GetAll()
        {
            var rawStaffOrders = _unitOfWork.RawStaffOrdersRepository.GetAll()
                .Include(rso => rso.IdSupplierNavigation)
                .Include(rso => rso.IdOrderStatusNavigation).ToList();
            var rawStaffOrdersDTO = _mapper.Map<List<RawStaffOrders>, List<RawStaffOrderDTO>>(rawStaffOrders);

            return rawStaffOrdersDTO;
        }

        public List<RawStaffOrderDTO> GetBySupplierId(int supplierId)
        {
            var rawStaffOrders = _unitOfWork.RawStaffOrdersRepository.GetAll()
                    .Include(rso => rso.IdSupplierNavigation)
                    .Include(rso => rso.IdOrderStatusNavigation)
                    .Where(order => order.IdSupplier == supplierId).ToList();
            var rawStaffOrdersDTO = _mapper.Map<List<RawStaffOrders>, List<RawStaffOrderDTO>>(rawStaffOrders);

            return rawStaffOrdersDTO;
        }

        public RawStaffOrderDTO GetById(int id)
        {
            var rawStaffOrder = _unitOfWork.RawStaffOrdersRepository.GetById(id);
            var rawStaffOrderDTO = _mapper.Map<RawStaffOrders, RawStaffOrderDTO>(rawStaffOrder);

            return rawStaffOrderDTO;

        }

        public List<EmployeeDTO> GetEmployeesByOrderId(int id)
        {
            var EmployeesId = _unitOfWork.EmployeesRawStaffOrdersRepository.GetAll()
                    .Include(empRawStaff => empRawStaff.IdEmployeeNavigation)
                    .Include(empRawStaff => empRawStaff.IdOrderNavigation)
                    .Where(prod => prod.IdOrder == id).ToList();
            var Employees = new List<Employees>();
            EmployeesId.ForEach(Emp =>
            {
                var Employee = _unitOfWork.EmployeesRepository.GetById(Emp.IdEmployee);
                Employees.Add(Employee);
            });

            return _mapper.Map<List<Employees>, List<EmployeeDTO>>(Employees);
        }

        public RawStaffOrderDTO Insert(RawStaffOrders order)
        {
            var supplier = _unitOfWork.SuppliersRepository.GetById(order.IdSupplier);
            order.Time = DateTime.UtcNow;
            if (supplier == null)
            {
                if (order.IdSupplierNavigation.IdAddressNavigation == null)
                    throw new Exception("Could not create supplier, no address");
            }
            _unitOfWork.RawStaffOrdersRepository.Add(order);
            _unitOfWork.Save();
            var orderDTO = _mapper.Map<RawStaffOrders, RawStaffOrderDTO>(order);

            return orderDTO;
        }


        public RawStaffOrderDTO Update(RawStaffOrders order)
        {
            _unitOfWork.RawStaffOrdersRepository.Edit(order);
            _unitOfWork.Save();
            var orderDTO = _mapper.Map<RawStaffOrders, RawStaffOrderDTO>(order);

            return orderDTO;
        }
    }
}
