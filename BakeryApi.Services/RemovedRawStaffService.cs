﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class RemovedRawStaffService : IRemovedRawStaffService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public RemovedRawStaffService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _unitOfWork.RemovedRawStaffRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<RemovedRawStaffDTO> GetAll()
        {
            var rawStaff = _unitOfWork.RemovedRawStaffRepository.GetAll()
                .Include(rs => rs.IdEmployeeNavigation)
                    .ThenInclude(empl => empl.IdAddressNavigation)
                .Include(rs => rs.IdEmployeeNavigation)
                    .ThenInclude(empl => empl.IdPositionNavigation)
                .Include(rs => rs.IdRawStaffNavigation)
                    .ThenInclude(rs => rs.IdMeasureNavigation)
                .ToList();
            return _mapper.Map<List<RemovedRowStaff>, List<RemovedRawStaffDTO>>(rawStaff);
        }

        public RemovedRawStaffDTO GetById(int id)
        {
            var rawStaff = _unitOfWork.RemovedRawStaffRepository.GetById(id);
            return _mapper.Map<RemovedRowStaff, RemovedRawStaffDTO>(rawStaff);
        }

        public RemovedRawStaffDTO RemoveFromStorage(RemovedRawStaffDTO removedRawStaff, int rawStaffOnStorageId)
        {
            var rawStaffOnStorage = _unitOfWork.RawStaffOnStorageRepository.GetById(rawStaffOnStorageId);
            removedRawStaff.IdRawStaff = rawStaffOnStorage.IdRawStaff;
            removedRawStaff.RemovingDate = DateTime.UtcNow;
            if (rawStaffOnStorage.Amount < removedRawStaff.Amount)
                throw new Exception("Not enough raw staff on storage");

            if (rawStaffOnStorage.Amount == removedRawStaff.Amount)
                _unitOfWork.RawStaffOnStorageRepository.Delete(rawStaffOnStorageId);

            if (rawStaffOnStorage.Amount > removedRawStaff.Amount)
            {
                rawStaffOnStorage.Amount -= removedRawStaff.Amount;
                _unitOfWork.RawStaffOnStorageRepository.Edit(rawStaffOnStorage);
            }

            var remRawStaff = _mapper.Map<RemovedRawStaffDTO, RemovedRowStaff>(removedRawStaff);
            _unitOfWork.RemovedRawStaffRepository.Add(remRawStaff);
            _unitOfWork.Save();

            removedRawStaff.IdRemovedRowStaff = remRawStaff.IdRemovedRowStaff;

            return removedRawStaff;
        }
    }
}
