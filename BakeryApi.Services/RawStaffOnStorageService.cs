﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class RawStaffOnStorageService: IRawStaffOnStorageService
    {

        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public RawStaffOnStorageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.ProductOnStorageRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<RawStaffOnStorageDTO> GetAll()
        {
            var rawStaffOnStorage = _unitOfWork.RawStaffOnStorageRepository.GetAll()
                    .Include(rsOnStrg => rsOnStrg.IdRawStaffNavigation)
                        .ThenInclude(rs => rs.IdMeasureNavigation)
                    .Include(rsOnStrg => rsOnStrg.IdStorageNavigation)
                        .ThenInclude(storage => storage.IdAddressNavigation)
                    .ToList();
            var rawStaffOnStorageDTO = _mapper.Map<List<RawStaffOnStorage>, List<RawStaffOnStorageDTO>>(rawStaffOnStorage);

            return rawStaffOnStorageDTO;
        }

        public RawStaffOnStorageDTO GetById(int id)
        {
            var rawStaffOnStorage = _unitOfWork.RawStaffOnStorageRepository.GetById(id);
            var rawStaffOnStorageDTO = _mapper.Map<RawStaffOnStorage, RawStaffOnStorageDTO>(rawStaffOnStorage);

            return rawStaffOnStorageDTO;
        }

        public List<EmployeeDTO> GetEmployeesByRawStaffId(int id)
        {
            var EmployeesId = _unitOfWork.EmployeesRawStaffOnStorageRepository.GetAll()
                .Include(emp => emp.IdEmployeeNavigation)
                .Include(emp => emp.IdRawStaffOnStorageNavigation)
                .Where(rawStaff => rawStaff.IdRawStaffOnStorage == id).ToList();
            var Employees = new List<Employees>();
            EmployeesId.ForEach(Emp =>
            {
                var Employee = _unitOfWork.EmployeesRepository.GetById(Emp.IdEmployee);
                Employees.Add(Employee);
            });

            return _mapper.Map<List<Employees>, List<EmployeeDTO>>(Employees);
        }

        public RawStaffOnStorageDTO Insert(InsertingRawStaffOnStorageDTO rawStaffDTO)
        {
            var rawStaffOnStorageDTO = rawStaffDTO.rawStaffOnStorage;
            var employeesId = rawStaffDTO.employeesId;
            //var ExpirationDate = _unitOfWork.RawStaffRepository.GetById(rawStaffOnStorageDTO.IdRawStaff).ExpirationDate;

            //rawStaffOnStorageDTO.ConsumeUntil = DateTime.UtcNow.AddDays((double)ExpirationDate);
            var rawStaffOnStorage = _mapper.Map<RawStaffOnStorageDTO, RawStaffOnStorage>(rawStaffOnStorageDTO);
            _unitOfWork.RawStaffOnStorageRepository.Add(rawStaffOnStorage);
            _unitOfWork.Save();

            employeesId.ForEach(id =>
            {
                _unitOfWork.EmployeesRawStaffOnStorageRepository.Add(new EmployeesRawStaffOnStorage()
                {
                    IdEmployee = id,
                    IdRawStaffOnStorage = rawStaffOnStorageDTO.IdRawStaffOnStorage
                });
            });
            _unitOfWork.Save();

            rawStaffOnStorageDTO.IdRawStaffOnStorage = rawStaffOnStorage.IdRawStaffOnStorage;

            return rawStaffOnStorageDTO;
        }

        public RawStaffOnStorageDTO Update(RawStaffOnStorageDTO rawStaffOnStorageDTO)
        {
            throw new NotImplementedException();
        }
    }
}
