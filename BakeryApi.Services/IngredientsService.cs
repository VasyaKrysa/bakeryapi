﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class IngredientsService:IIngredientsService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public IngredientsService(IUnitOfWork unitOfWork, IMapper mapper) 
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.IngredientsRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<IngredientDTO> GetAll()
        {
            var ingredients= _unitOfWork.IngredientsRepository.GetAll().ToList();
            var ingredientsDTO = _mapper.Map<List<Ingredients>, List<IngredientDTO>>(ingredients);
            ingredientsDTO.ForEach(ing =>
            {
                ing.MeasureName = _unitOfWork.MeasureRepository.GetById(ing.IdMeasure).Name;
                ing.ProductName = _unitOfWork.ProductsRepository.GetById(ing.IdProduct).ProductName;
                ing.RawStaffName = _unitOfWork.RawStaffRepository.GetById(ing.IdRawStaff).Name;
            });

            return ingredientsDTO;
        }

        public IngredientDTO GetById(int id)
        {
            var ingredient = _unitOfWork.IngredientsRepository.GetById(id);
            var ingredientDTO = _mapper.Map<Ingredients, IngredientDTO>(ingredient);
           
            ingredientDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(ingredientDTO.IdMeasure).Name;
            ingredientDTO.ProductName = _unitOfWork.ProductsRepository.GetById(ingredientDTO.IdProduct).ProductName;
            ingredientDTO.RawStaffName = _unitOfWork.RawStaffRepository.GetById(ingredientDTO.IdRawStaff).Name;

            return ingredientDTO;
        }

        public List<IngredientDTO> GetByProductId(int productId)
        {
            var ingredients = _unitOfWork.IngredientsRepository.GetAll().Where(ing=>ing.IdProduct==productId).ToList();
            var ingredientsDTO = _mapper.Map<List<Ingredients>, List<IngredientDTO>>(ingredients);
            ingredientsDTO.ForEach(ing =>
            {
                ing.MeasureName = _unitOfWork.MeasureRepository.GetById(ing.IdMeasure).Name;
                ing.ProductName = _unitOfWork.ProductsRepository.GetById(ing.IdProduct).ProductName;
                ing.RawStaffName = _unitOfWork.RawStaffRepository.GetById(ing.IdRawStaff).Name;
            });

            return ingredientsDTO;
        }

        public IngredientDTO Insert(IngredientDTO ingredient)
        {
            var Ingredient = _mapper.Map<IngredientDTO, Ingredients>(ingredient);
            _unitOfWork.IngredientsRepository.Add(Ingredient);
            _unitOfWork.Save();
            ingredient.IdIngredient = Ingredient.IdIngredient;

            return ingredient;
        }

        public List<IngredientDTO> InsertRange(List<IngredientDTO> ingredient)
        {
            var ingredients = _mapper.Map<List<IngredientDTO>, List<Ingredients>>(ingredient);
            ingredients.ForEach(ing => _unitOfWork.IngredientsRepository.Add(ing));
            _unitOfWork.Save();
            for(int i=0;i<ingredients.Count;i++)
            {
                ingredient[i].IdIngredient = ingredients[i].IdIngredient;
            }

            return ingredient;
        }

        public IngredientDTO Update(IngredientDTO ingredient)
        {
            var Ingredient = _mapper.Map<IngredientDTO, Ingredients>(ingredient);
            _unitOfWork.IngredientsRepository.Edit(Ingredient);
            _unitOfWork.Save();

            return ingredient;
        }
    }
}
