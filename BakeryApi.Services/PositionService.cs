﻿using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BakeryApi.Services
{
    public class PositionService : IPositionsService
    {
        private IUnitOfWork _unitOfWork;

        public PositionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _unitOfWork.PositionRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<Positions> GetAll()
        {
            return _unitOfWork.PositionRepository.GetAll().ToList();
        }

        public Positions GetByName(string name)
        {
            return _unitOfWork.PositionRepository.GetAll().Where(position => position.PositionName == name).FirstOrDefault();
        }

        public Positions Insert(Positions position)
        {
            _unitOfWork.PositionRepository.Add(position);
            _unitOfWork.Save();

            return position;
        }

        public Positions Update(Positions position)
        {
            _unitOfWork.PositionRepository.Edit(position);
            _unitOfWork.Save();

            return position;
        }
    }
}
