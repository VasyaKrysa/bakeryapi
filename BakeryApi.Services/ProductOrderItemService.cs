﻿using AutoMapper;
using BakeryApi.Core.Abstractions;
using BakeryApi.Core.Abstractions.Services;
using BakeryApi.Core.DTO;
using BakeryApi.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryApi.Services
{
    public class ProductOrderItemService:IProductOrderItemService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public ProductOrderItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.ProductOrderItemRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<ProductOrderItemDTO> GetAll()
        {
            var orderItems = _unitOfWork.ProductOrderItemRepository.GetAll().ToList();
            var orderItemsDTO = _mapper.Map<List<ProductOrderItems>, List<ProductOrderItemDTO>>(orderItems);
            orderItemsDTO.ForEach(item =>
            {
                var order = _unitOfWork.ProductOrdersRepository.GetById(item.IdOrder);
                var orderDTO = _mapper.Map<ProductOrders, ProductsOrderDTO>(order);
               // orderDTO.Customer = _unitOfWork.CustomersRepository.GetById(orderDTO.IdCustomer);
                item.Order = orderDTO;

                var product = _unitOfWork.ProductsRepository.GetById(item.IdProduct);
                var productDTO = _mapper.Map<Products, ProductDTO>(product);
                productDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(productDTO.IdMeasure).Name;
                item.Product = productDTO;
            });

            return orderItemsDTO;
        }

        public ProductOrderItemDTO GetById(int id)
        {
            var orderItem = _unitOfWork.ProductOrderItemRepository.GetById(id);
            var orderItemDTO = _mapper.Map<ProductOrderItems, ProductOrderItemDTO>(orderItem);
          
            var order = _unitOfWork.ProductOrdersRepository.GetById(orderItemDTO.IdOrder);
            var orderDTO = _mapper.Map<ProductOrders, ProductsOrderDTO>(order);
           // orderDTO.Customer = _unitOfWork.CustomersRepository.GetById(orderDTO.IdCustomer);
            orderItemDTO.Order = orderDTO;

            var product = _unitOfWork.ProductsRepository.GetById(orderItemDTO.IdProduct);
            var productDTO = _mapper.Map<Products, ProductDTO>(product);
            productDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(productDTO.IdMeasure).Name;
            orderItemDTO.Product = productDTO;
            
            return orderItemDTO;
        }

        public List<ProductOrderItemDTO> GetByOrderId(int orderId)
        {
            var orderItems = _unitOfWork.ProductOrderItemRepository.GetAll().Where(prod=>prod.IdOrder==orderId).ToList();
            var orderItemsDTO = _mapper.Map<List<ProductOrderItems>, List<ProductOrderItemDTO>>(orderItems);
            orderItemsDTO.ForEach(item =>
            {
                var order = _unitOfWork.ProductOrdersRepository.GetById(item.IdOrder);
                var orderDTO = _mapper.Map<ProductOrders, ProductsOrderDTO>(order);
               // orderDTO.Customer = _unitOfWork.CustomersRepository.GetById(orderDTO.IdCustomer);
                item.Order = orderDTO;

                var product = _unitOfWork.ProductsRepository.GetById(item.IdProduct);
                var productDTO = _mapper.Map<Products, ProductDTO>(product);
                productDTO.MeasureName = _unitOfWork.MeasureRepository.GetById(productDTO.IdMeasure).Name;
                item.Product = productDTO;
            });

            return orderItemsDTO;
        }

        public ProductOrderItemDTO Insert(ProductOrderItemDTO orderItem)
        {
            var OrderItem = _mapper.Map<ProductOrderItemDTO, ProductOrderItems>(orderItem);
            _unitOfWork.ProductOrderItemRepository.Add(OrderItem);
            _unitOfWork.Save();
            orderItem.IdProductOrderItem = OrderItem.IdProductOrderItem;

            return orderItem;
        }

        public List<ProductOrderItemDTO> InsertRange(List<ProductOrderItemDTO> orderItems)
        {
            var OrderItems = _mapper.Map < List<ProductOrderItemDTO>, List<ProductOrderItems>>(orderItems);
            OrderItems.ForEach(item =>
            {
                _unitOfWork.ProductOrderItemRepository.Add(item);
            });
            _unitOfWork.Save();
            for (int i=0;i<orderItems.Count;i++)
            {
                orderItems[i].IdProductOrderItem = OrderItems[i].IdProductOrderItem;
            }

            return orderItems;
        }

        public ProductOrderItemDTO Update(ProductOrderItemDTO orderItem)
        {
            var OrderItem = _mapper.Map<ProductOrderItemDTO, ProductOrderItems>(orderItem);
            _unitOfWork.ProductOrderItemRepository.Edit(OrderItem);
            _unitOfWork.Save();

            return orderItem;
        }
    }
}
